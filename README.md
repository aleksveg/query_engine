# <ins>README - Query Engine and Viewer</ins>

Bachelor thesis developed in collaboration with Postnord. The purpose of the project
is to create a simple web application for extracting data in a simple manner
without having to know SQL as a prerequisite. 

## Why this project 
Extracting data from a database can be a time-consuming and expensive process. To 
free up developers to focus on writing code and implement new features instead,
this page is created. In this manner both administrators and developers can access
data quickly, leaving more time for coding and other activities. 

## 


## Installation 
This application is the backend of the Common Query Engine and Viewer application that runs in the background without any form for graphical user interface.

### Packaging
Run "mvn clean package" in the terminal to create a .jar file that can be executed using a JDK.

### Building docker image
Run "docker build -t query_engine ." in the terminal to build the docker image.

### Starting the docker container
To be able to start the docker container run "docker run --name "spring" -d -p 8080:8080" in the terminal.


### Important note!
* The Dockerfile must be in the same folder as the jar.
* The backend is set up to use port 8080.
* To open the GraphQL terminal, go to "$URL:8080/graphiql". Please note that $URL must be replaced with the ip address of the server running the image.
* By running GraphQL queries towards "http://$URL:8080/graphql" you are able to fetch data. Please note that $URL must be replaced with the ip address of the server running the image.