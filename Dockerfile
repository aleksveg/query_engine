# Build stage
FROM openjdk:17-jdk-slim-buster as builder
# Setting the working directory directory
WORKDIR /app
# Copy maven wrapper files
COPY mvnw .
COPY .mvn .mvn

COPY pom.xml .

RUN ./mvnw dependency:go-offline
COPY src src

RUN ./mvnw package

# Production stage
FROM openjdk:17-jdk-slim-buster as production-stage
# Set working directory
WORKDIR /app
RUN apt-get update && apt-get install -y git
# Copy the built Jar file from build-stage
COPY --from=builder /app/target/*.jar app.jar
# Exposes the port
EXPOSE 8080
# Set the entrypoint to run the application
ENTRYPOINT ["java", "-jar", "app.jar" ]
