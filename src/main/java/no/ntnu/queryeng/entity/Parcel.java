package no.ntnu.queryeng.entity;

import jakarta.persistence.*;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

import no.ntnu.queryeng.tools.InputChecker;

/**
 * Represents a parcel-object in the query engine application.
 *
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 25.04.2023
 */
@Entity
@Table(name = "parcel")
public class Parcel {

    /***
     * The ID of the parcel.
     */
    @jakarta.persistence.Id
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;


    /**
     * The weight of the parcel.
     */
    @Column(name = "weight")
    private double weight;

    /**
     * The shipping order id the parcel is connected to the parcel
     */
    @ManyToOne
    @JoinColumn(name = "shipping_order_id")
    private ShippingOrder shippingOrder;

    /**
     * The shipping cost id connected to the parcel.
     */
    @ManyToOne
    @JoinColumn(name = "shipping_cost_id")
    private ShippingCost shippingCost;

    /**
     * The parcels barcode.
     */
    @OneToOne
    @JoinColumn(name = "barcode_id")
    private Barcode barcode;

    /**
     * List to hold all the scans for the given parcel
     */
    @OneToMany(mappedBy = "parcel")
    private List<Scan> scans;

    /**
     * Creates an instance of a parcel-object.
     *
     * @param id     the id of the parcel.
     * @param weight the weight of the parcel.
     * @throws IllegalArgumentException if some inputs is not correct.
     */
    public Parcel(int id, double weight) {
        if (InputChecker.isValid(id) && InputChecker.isValid(weight)) {
            this.id = id;
            this.weight = weight;
            this.scans = new ArrayList<>();
        } else {
            throw new IllegalArgumentException("Invalid input");
        }
    }

    /**
     * No args constructor. Intentionally left empty.
     */
    public Parcel() {
    }

    /**
     * Returns the ID of the parcel.
     * @return the ID of the parcel.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Return the trackingNumber of the parcel.
     * @return the trackingNumber of the parcel.
     */
    public double getWeight() {
        return this.weight;
    }


    /**
     * Return the shipping order connected to this parcel.
     * @return the shipping order connected to this parcel.
     */
    public ShippingOrder getShippingOrder() {
        return this.shippingOrder;
    }

    /**
     * Return the barcode of the parcel.
     * @return the barcode of the parcel.
     **/
    public Barcode getBarcode() {
        return this.barcode;
    }

    /**
     * Returns theshipping cost of this parcel.
     * @return the shipping cost of this parcel.
     **/
    public ShippingCost getShippingCost() {
        return this.shippingCost;
    }

    /**
     * Returns the list of scans for the given parcel.
     * @return list of scans for this parcel.
     */
    public List<Scan> getScans() {
        return this.scans;
    }
}
