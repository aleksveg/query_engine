package no.ntnu.queryeng.entity;

import jakarta.persistence.*;
import no.ntnu.queryeng.tools.InputChecker;

import java.util.ArrayList;
import java.util.List;


/**
 * Represents a third party table in the database.
 * Third party refers to the other payers than sender and receiver.
 *
 * @author Aleksander Vegsund, Balendra Sounthararajan, Joakim Røren Melum
 * @version 0.1
 * @date 25.04.2023
 */
@Entity
@Table(name = "third_party")
public class ThirdParty {

    /**
     * The third party's unique id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    /**
     * The third party's name.
     */
    @Column(name = "name")
    private String name;

    /**
     * The third party's address.
     */
    @Column(name = "address")
    private String address;

    /**
     * The third party's postal code.
     */
    @Column(name = "post_code")
    private int postalCode;

    /**
     * The third party's mobile number.
     */
    @Column(name = "mobile")
    private int mobile;

    /**
     * The third party's email.
     */
    @Column(name = "email")
    private String email;

    /**
     * The third party's lined invoices.
     */
    @OneToMany(mappedBy = "thirdParty")
    private List<Invoice> invoices;

    /**
     * The third party's linked invoices related to additional services..
     */
    @OneToMany(mappedBy = "thirdParty")
    private List<AdditionalServiceInvoice> additionalServiceInvoices;

    /**
     * No args constructor. Intentionally left empty.
     */
    public ThirdParty() {
    }

    /**
     * Creates an instance of the ThirdParty-object.
     *
     * @param id         the third party's unique id.
     * @param name       the third party's name.
     * @param address    the third party's address.
     * @param postalCode the third party's postal code.
     * @param mobile     the third party's mobile number.
     * @param email      the third party's email.
     * @throws IllegalArgumentException if some inputs is not correct.
     */
    public ThirdParty(int id, String name, String address, int postalCode, int mobile, String email) {
        if (InputChecker.isValid(id) &&
                InputChecker.isValid(name) &&
                InputChecker.isValid(address) &&
                InputChecker.isValid(postalCode) &&
                InputChecker.isValid(mobile) &&
                InputChecker.isValid(email)) {
            this.id = id;
            this.name = name;
            this.address = address;
            this.postalCode = postalCode;
            this.mobile = mobile;
            this.email = email;
            this.invoices = new ArrayList<>();
            this.additionalServiceInvoices = new ArrayList<>();
        } else {
            throw new IllegalArgumentException("Invalid input");
        }
    }

    /**
     * Returns the ID of the ThirdParty-object.
     * @return the ID of the ThirdParty-object.
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the name of the ThirdParty-object.
     * @return the name of the ThirdParty-object.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the address of the ThirdParty-object.
     * @return the address of the ThirdParty-object.
     */
    public String getAddress() {
        return address;
    }

    /**
     * Returns the postal code of the ThirdParty-object.
     * @return the  postal code of the ThirdParty-object.
     */
    public int getPostalCode() {
        return postalCode;
    }

    /**
     * Returns the  mobile number of the ThirdParty-object.
     * @return the  mobile number of the ThirdParty-object.
     */
    public int getMobile() {
        return mobile;
    }

    /**
     * Returns the  email address of the ThirdParty-object.
     * @return the  email address of the ThirdParty-object.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Returns the invoices related to the of the ThirdParty-object.
     * @return the invoices related to the of the ThirdParty-object.
     */
    public List<Invoice> getInvoices() {
        return invoices;
    }

    /**
     * Returns the invoices related to the additional services linked to the ThirdParty-object.
     * @return the invoices related to the additional services linked to the ThirdParty-object.
     */
    public List<AdditionalServiceInvoice> getAdditionalServiceInvoices() {
        return additionalServiceInvoices;
    }
}
