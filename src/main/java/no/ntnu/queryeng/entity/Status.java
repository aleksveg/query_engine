package no.ntnu.queryeng.entity;

import jakarta.persistence.*;
import no.ntnu.queryeng.tools.InputChecker;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents the Constructor of a Status entity.
 * It's content features the different attributes the Status table has in the DB
 *
 * @author Odin Ulvestad, Joakim Røren Melum
 * @version 0.1
 * @date 25.04.2023
 */
@Entity
@Table(name = "Status")
public class Status {
    /**
     * The status unique ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    /**
     * The status itself.
     */
    @Column(name = "status")
    private String status; // The status of which the parcel/order may hold

    /**
     * The scans linked to the status.
     */
    @OneToMany
    @JoinColumn(name = "status_id")
    private List<Scan> scans;

    /**
     * No args constructor. Intentionally left empty.
     */
    public Status() {
    }

    /**
     * Constructor for Status. Creates an instance of the Status Object with the following
     * parameters: id and status
     *
     * @param id     The unique ID for the status
     * @param status The status of which a parcel/order contains
     * @throws IllegalArgumentException if some inputs is not correct.
     */
    public Status(int id, String status) {
        if (InputChecker.isValid(id) && InputChecker.isValid(status)) {
            this.id = id;
            this.status = status;
            this.scans = new ArrayList<>();
        } else {
            throw new IllegalArgumentException("Invalid input");
        }
    }

    /**
     * Returns the ID of the status instance.
     * @return the ID of the status instance.
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the name of the status instance.
     * @return the name of the status instance.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Returns the list of scans with the given status.
     * @return the list of scans with given status.
     */
    public List<Scan> getScans() {
        return this.scans;
    }
}
