package no.ntnu.queryeng.entity;

import jakarta.persistence.*;
import no.ntnu.queryeng.tools.InputChecker;

import java.sql.Date;

/**
 * Class represents a scan that is done for each parcel at each control point on the way from sender to final
 * destination.
 */
@Entity
@Table(name = "scan")
public class Scan {

    /**
     * The scan's unique ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    /**
     * The scan's linked parcel.
     */
    @ManyToOne
    @JoinColumn(name = "parcel_id")
    private Parcel parcel;


    /**
     * The scan's linked terminal.
     */
    @ManyToOne
    @JoinColumn(name = "terminal_id")
    private Terminal terminal;


    /**
     * The scan's linked status.
     */
    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;


    /**
     * The scan's date and time.
     */
    @Column(name = "scanned_at")
    private Date scannedAt;

    /**
     * Constructs an instance of this class
     *
     * @param id        unique id of this particular scan
     * @param parcel    the parcel that is scanned
     * @param terminal  the terminal where the scan is executed
     * @param status    the status of the given parcel
     * @param scannedAt the date and time the scan was executed
     * @throws IllegalArgumentException if some inputs is not correct.
     */
    public Scan(int id, Parcel parcel, Terminal terminal, Status status, Date scannedAt) {
        if (InputChecker.isValid(id) && parcel != null && terminal != null && status != null && scannedAt != null) {
            this.id = id;
            this.parcel = parcel;
            this.terminal = terminal;
            this.status = status;
            this.scannedAt = scannedAt;
        } else {
            throw new IllegalArgumentException("one or more parameters have an invalid value");
        }
    }

    /**
     * No args constructor. Intentionally left empty..
     */
    public Scan() {
    }

    /**
     * Returns the ID of the scan.
     *
     * @return the ID of the scan.
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the parcel.
     *
     * @return the parcel.
     */
    public Parcel getParcel() {
        return this.parcel;
    }

    /**
     * Returns the terminal.
     *
     * @return the terminal.
     */
    public Terminal getTerminal() {
        return terminal;
    }

    /**
     * Returns the status.
     *
     * @return the status.
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Returns the date and time when the scan occurred.
     *
     * @return the date and time when the scan occurred
     */
    public Date getScannedAt() {
        return scannedAt;
    }
}
