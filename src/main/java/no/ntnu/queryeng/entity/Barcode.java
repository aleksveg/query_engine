package no.ntnu.queryeng.entity;

import jakarta.persistence.*;
import no.ntnu.queryeng.tools.InputChecker;

/**
 * This class represents a barcode in the system.
 *
 * @author Aleksander Vegsund, Joakim Røren Melum
 * @version 0.1
 * @date 25.04.2023
 */

@Entity
@Table(name = "barcode")
public class Barcode {

    /**
     * The objects unique ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * The barcode.
     */
    @Column(name = "barcode")
    private String barcode;

    /**
     * Holds the status if the barcode is in use or not.
     */
    @Column(name = "in_use")
    private boolean inUse;

    /**
     * Holds the connected parcel that is connected to the barcode.
     */
    @OneToOne(mappedBy = "barcode")
    private Parcel parcel;


    /**
     * Constructs an instance of this class
     *
     * @param id      the ID of the barcode
     * @param barcode the barcode itself.
     * @param inUse   tells if the barcode is in use or not.
     */
    public Barcode(int id, String barcode, boolean inUse) {
        if (InputChecker.isValid(id) && InputChecker.isValid(barcode)) {
            this.id = id;
            this.barcode = barcode;
            this.inUse = inUse;
        } else {
            throw new IllegalArgumentException("Invalid input");
        }
    }

    /**
     * No args constructor. Intentionally left empty.
     */
    public Barcode() {

    }

    /**
     * Returns the ID of the barcode.
     * @return the ID of the barcode.
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the barcode.
     * @return the barcode.
     */
    public String getBarcode() {
        return barcode;
    }

    /**
     * Returns <code>TRUE</code> if the barcode is already in use.
     * <code>FALSE</code> will be returned if the barcode is not in use.
     * @return <code>TRUE</code> if the barcode is already in use. <code>FALSE</code> will be returned if the barcode is not in use.
     */
    public boolean isInUse() {
        return inUse;
    }

    /**
     * Returns the parcel connected to the barcode
     * @return the parcel connected to the barcode.
     */
    public Parcel getParcel() {
        return this.parcel;
    }
}
