package no.ntnu.queryeng.entity;

import jakarta.persistence.*;
import no.ntnu.queryeng.tools.InputChecker;

import java.time.LocalDateTime;

/**
 * This class represents an additional service invoice in the system.
 * An additional service invoice contains the following about what
 * additional services that is placed on the order. Who is the invoice
 * payer, the date it was created and due date of the invoice.
 * It also contains information that tells if the invoice is
 * paid or not.
 *
 * @author aleksander & Joakim Røren Melum
 * @version 0.1
 * @date 25.04.2023
 */

@Entity
@Table(name = "additional_service_invoice")
public class AdditionalServiceInvoice {

    /**
     * The additional service invoice's ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    /**
     * The additional service invoice's linked additional service order.
     */
    @OneToOne(mappedBy = "additionalServiceInvoice")
    private AdditionalServiceOrder additionalServiceOrder;

    /**
     * The additional service invoice's linked customer.
     */
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    /**
     * The additional service invoice's linked third party.
     */
    @ManyToOne()
    @JoinColumn(name = "third_party_id")
    private ThirdParty thirdParty;

    /**
     * The additional service invoice's payer type.
     */
    @ManyToOne
    @JoinColumn(name = "payer_type_id")
    private InvoicePayerType invoicePayerType;

    /**
     * The additional service invoice's created at.
     */
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    /**
     * The additional service invoice's.
     */
    @Column(name = "due_date")
    private LocalDateTime dueDate;

    /**
     * The additional service invoice's.
     */
    @Column(name = "is_paid")
    private boolean isPaid;

    /**
     * Creates an instance of this class.
     *
     * @param id the additional service invoice's id.
     * @param additionalServiceOrder the additional service order.
     * @param createdAt              the time and date the invoice is created.
     * @param dueDate                the date the invoice the is due.
     * @param isPaid                 indicates if the invoice is paid or not.
     */
    public AdditionalServiceInvoice(int id, AdditionalServiceOrder additionalServiceOrder,
                                    LocalDateTime createdAt, LocalDateTime dueDate,
                                    boolean isPaid) {
        if (InputChecker.isValid(id) && additionalServiceOrder != null && createdAt != null && dueDate != null) {
            this.id = id;
            this.additionalServiceOrder = additionalServiceOrder;
            this.createdAt = createdAt.now();
            this.dueDate = dueDate;
            this.isPaid = isPaid;

        } else {
            throw new IllegalArgumentException("Invalid input");
        }
    }

    /**
     * No args constructor. Intentionally left empty.
     */
    public AdditionalServiceInvoice() {

    }

    /**
     * Return the ID of the additional service invoice.
     * @return the ID of the additional service invoice.
     */
    public int getId() {
        return id;
    }

    /**
     * Return the linked additional service order.
     * @return the linked additional service order.
     */
    public AdditionalServiceOrder getAdditionalServiceOrder() {
        return additionalServiceOrder;
    }

    /**
     * Return the linked customer id.
     * @return the linked customer id.
     */
    public Customer getCustomerId() {
        return customer;
    }

    /**
     * Return the linked third party.
     * @return the linked third party.
     */
    public ThirdParty getThirdParty() {
        return thirdParty;
    }

    /**
     * Return the linked invocie payer type id.
     * @return the linked invocie payer type id.
     */
    public InvoicePayerType getInvoicePayerTypeId() {
        return invoicePayerType;
    }
    /**
     * Return the date and time the additional service invoice was created at.
     * @return the date and time the additional service invoice was created at.
     */
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    /**
     * Return the date and time the additional service invoice is due at.
     * @return the date and time the additional service invoice is due at.
     */
    public LocalDateTime getDueDate() {
        return dueDate;
    }

    /**
     * Return if the additional service invoice is paid or not.
     * @return if the additional service invoice is paid or not.
     */
    public boolean isPaid() {
        return isPaid;
    }
}
