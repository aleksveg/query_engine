package no.ntnu.queryeng.entity;

import jakarta.persistence.*;
import no.ntnu.queryeng.tools.InputChecker;

import java.time.LocalDateTime;

/**
 * The class represents and an addition service order in the system.
 * An additional service order contains a reference to the shipping
 * order, the given additional service and the date and time it
 * was ordered.
 *
 * @author Aleksander Vegsund, Joakim Røren Melum
 * @version 0.1
 * @date 25.04.2023
 */
@Entity
@Table(name = "additional_service_order")
public class AdditionalServiceOrder {

    /**
     * The objects unique ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * The linked shipping order ID.
     */
    @Column(name = "shipping_order_id")
    private int shippingOrderId;

    /**
     * The linked additional service.
     */
    @OneToOne(mappedBy = "additionalServiceOrder")
    private AdditionalService additionalService;

    /**
     * The linked additional service invoice.
     */
    @OneToOne
    @JoinColumn(name = "additional_service_invoice_id")
    private AdditionalServiceInvoice additionalServiceInvoice;

    /**
     * The date and time the additional service order was created.
     */
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    /**
     * Creates an instance of this class
     *
     * @param id                the id of the additional service order
     * @param shippingOrderId   the id of the shipping order
     * @param additionalService the additional service
     * @param createdAt         the time and date the additional service order was created
     * @throws IllegalArgumentException if some inputs is not correct.
     */
    public AdditionalServiceOrder(int id, int shippingOrderId, AdditionalService additionalService, LocalDateTime createdAt) {
        if (InputChecker.isValid(id) && InputChecker.isValid(shippingOrderId) && additionalService != null && createdAt != null) {
            this.id = id;
            this.shippingOrderId = shippingOrderId;
            this.additionalService = additionalService;
            this.createdAt = createdAt.now();
        } else {
            throw new IllegalArgumentException("Invalid input");
        }
    }

    /**
     * No args constructor. Intentionally left empty.
     */
    public AdditionalServiceOrder() {

    }

    /**
     * Returns the ID of the additional service order.
     * @return the ID of the additional service order.
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the shipping order id of the additional service order.
     * @return the shipping order id of the additional service order.
     */
    public int getShippingOrderId() {
        return shippingOrderId;
    }

    /**
     * Returns the linked additional service.
     * @return the linked additional service.
     */
    public AdditionalService getAdditionalService() {
        return additionalService;
    }

    /**
     * Returns the additional service invoice.
     * @return the additional service invoice.
     */
    public AdditionalServiceInvoice getAdditionalServiceInvoice() {
        return this.additionalServiceInvoice;
    }

    /**
     * Returns the date and time the additional service order was created at.
     * @return the date and time the additional service order was created at.
     */
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
}
