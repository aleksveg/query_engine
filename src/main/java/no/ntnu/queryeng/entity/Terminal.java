package no.ntnu.queryeng.entity;

import jakarta.persistence.*;
import no.ntnu.queryeng.tools.InputChecker;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents the Constructor of a Terminal entity.
 * It's content features the different attributes the Terminal table has in the DB
 *
 * @author Odin Ulvestad, Joakim Røren Melum
 * @version 0.1
 * @date 25.04.2023
 */
@Entity
@Table(name = "terminal")
public class Terminal {

    /**
     * The terminal ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private int id;

    /**
     * The terminal's name.
     */
    @Column (name = "name")
    private String name;

    /**
     * The terminal address.
     */
    @Column (name = "address")
    private String address;

    /**
     * The terminal postal code.
     */
    @Column (name = "post_code")
    private int postalCode;

    /**
     * The terminal postal name.
     */
    @Column (name = "postal_name")
    private String postalName;

    /**
     * The terminal latitude position.
     */
    @Column (name = "latitude")
    private float position_latitude;

    /**
     * The terminal longitude position.
     */
    @Column (name = "longitude")
    private float position_longitude;

    /**
     * The cost of the scan a Terminal executes.
     */
    @Column (name = "cost")
    private int cost;

    /**
     * The terminal ID.
     */
    @OneToMany(mappedBy = "terminal")
    private List<Scan> scans;

    /**
     * No args constructor. Intentionally left empty.
     */
    public Terminal () {
    }

    /**
     * Constructor for Terminal! Creates an instance of the Terminal object with the following
     * parameters: id, name, address, cost.
     *
     * @param id the id of the terminal.
     * @param name the name of the terminal.
     * @param address the address of the terminal location.
     * @param postalCode the postal code for the terminal.
     * @param postalName the postal name for the terminal.
     * @param latitude the latitude position for the terminal.
     * @param longitude the longitude position for the terminal.
     * @param cost per scan the terminal executes.
     * @throws IllegalArgumentException if some inputs is not correct.
     */
    public Terminal (int id, String name, String address, int postalCode, String postalName, float latitude, float longitude, int cost) {
        if (InputChecker.isValid(id) &&
                InputChecker.isValid(name) &&
                InputChecker.isValid(address) &&
                InputChecker.isValid(postalCode) &&
                InputChecker.isValid(postalName) &&
                InputChecker.isValid(longitude) &&
                InputChecker.isValid(latitude) &&
                InputChecker.isValid(cost)){
            this.id = id;
            this.name = name;
            this.address = address;
            this.cost = cost;
            this.scans = new ArrayList<>();
            this.postalCode = postalCode;
            this.postalName = postalName;
            this.position_latitude = latitude;
            this.position_longitude = longitude;
        } else {
            throw new IllegalArgumentException("Invalid input");
        }
    }

    /**
     * Returns the terminal ID.
     * @return the terminal ID.
     */
    public int getId() { return id; }

    /**
     * Returns the terminal name.
     * @return the terminal name.
     */
    public String getName() { return name; }

    /**
     * Returns the terminal address.
     * @return the terminal address.
     */
    public String getAddress() { return address; }

    /**
     * Returns the terminal postal name.
     * @return the terminal postal name.
     */
    public String getPostalName() { return postalName; }

    /**
     * Returns the terminal postal code.
     * @return the terminal postal code.
     */
    public int getPostalCode() { return postalCode; }

    /**
     * Returns the terminal latitude position.
     * @return the terminal latitude position.
     */
    public double getLatitude() { return position_latitude; }

    /**
     * Returns the terminal longitude position.
     * @return the terminal longitude position.
     */
    public double getLongitude() { return position_longitude; }

    /**
     * Returns the cost of the scan a Terminal has.
     * @return the cost of the scan a Terminal has.
     */
    public int getCost() { return cost; }

    /**
     * Returns the list of scans done in this terminal.
     * @return the list of scans done by this terminal.
     */
    public List<Scan> getScans() {
        return this.scans;
    }

}
