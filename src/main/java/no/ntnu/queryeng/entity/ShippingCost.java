package no.ntnu.queryeng.entity;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

import static no.ntnu.queryeng.tools.InputChecker.*;

/**
 * Class represents shipping costs related to a parcel in this system.
 *
 * @author Aleksander Vegsund, Joakim Røren Melum
 * @version 1.0-SNAPSHOT
 */
@Entity
@Table(name = "shipping_cost")
public class ShippingCost {

    /**
     * The shipping cost's unique ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * The shipping cost's description.
     */
    @Column(name = "description")
    private String description;


    /**
     * The shipping cost.
     */
    @Column(name = "cost")
    private double cost;


    /**
     * The shipping cost's linked parcels.
     */
    @OneToMany(mappedBy = "shippingCost")
    private List<Parcel> parcels;

    /**
     * Constructs an instance of this class
     *
     * @param id          the shipping cost id
     * @param description describes what the cost is
     * @param cost        the price
     * @throws IllegalArgumentException if some inputs is not correct.
     */
    public ShippingCost(int id, String description, double cost) {
        if (isValid(id) && isValid(cost) && isValid(description)) {
            this.description = description;
            this.cost = cost;
            this.parcels = new ArrayList<>();
        } else {
            throw new IllegalArgumentException("One or more of the parameters have an invalid value");
        }
    }

    /**
     * No args constructor. Intentionally left empty.
     */
    public ShippingCost() {
    }

    /**
     * Returns the shipping cost's ID.
     * @return the shipping cost's ID.
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the shipping cost's description.
     * @return the shipping cost's description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the shipping cost
     * @return the shipping cost
     */
    public double getCost() {
        return cost;
    }

    /**
     * Returns the shipping cost's linked parcels.
     * @return the shipping cost's linked parcels.
     */
    public List<Parcel> getParcels() {
        return this.parcels;
    }
}
