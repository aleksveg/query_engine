package no.ntnu.queryeng.entity;

import jakarta.persistence.*;
import no.ntnu.queryeng.tools.InputChecker;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents an invoice payer type in the system.
 *
 * @author Aleksander Vegsund, Balendra Sounthararajan, Joakim Røren Melum
 * @version 0.1
 * @date 25.04.2023
 */
@Entity
@Table(name = "invoice_payer_type")
public class InvoicePayerType {

    /**
     * The objects unique ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * The objects payer type.
     */
    @Column(name = "payer_type")
    private String payerType;

    /**
     * The invoices linked to the invoice payer type object.
     */
    @OneToMany(mappedBy = "invoicePayerType")
    private List<Invoice> invoices;

    /**
     * The additional service invoices linked to the invoice payer type object.
     */
    @OneToMany(mappedBy = "invoicePayerType")
    private List<AdditionalServiceInvoice> additionalServiceInvoices;

    /**
     * Creates an instance of this class
     *
     * @param id        the id of the payer.
     * @param payerType the type of payer.
     * @throws IllegalArgumentException if some inputs is not correct.
     */
    public InvoicePayerType(int id, String payerType) {
        if (InputChecker.isValid(id) && InputChecker.isValid(payerType)) {
            this.payerType = payerType;
            this.invoices = new ArrayList<>();
            this.additionalServiceInvoices = new ArrayList<>();
        } else {
            throw new IllegalArgumentException("Invalid input");
        }
    }

    /**
     * No args constructor. Intentionally left empty..
     */
    public InvoicePayerType() {
    }

    /**
     * Returns the invoice payer type ID.
     * @return the invoice payer type ID.
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the invoice payer type.
     * @return the invoice payer type.
     */
    public String getPayerType() {
        return payerType;
    }

    /**
     * Returns the invoice payer types invoices.
     * @return the invoice payer type invoices.
     */
    public List<Invoice> getInvoices() {
        return this.invoices;
    }

    /**
     * Returns the invoice payer type linked additional service invoices.
     * @return the invoice payer type linked additional service invoices.
     */
    public List<AdditionalServiceInvoice> getAdditionalServiceInvoices() {
        return this.additionalServiceInvoices;
    }
}
