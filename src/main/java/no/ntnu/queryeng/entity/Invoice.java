package no.ntnu.queryeng.entity;

import jakarta.persistence.*;
import no.ntnu.queryeng.tools.InputChecker;

import java.sql.Date;

/**
 * Class represents an invoice object in the system.
 */

@Entity
@Table(name = "invoice")
public class Invoice {

    /**
     * The invoice's unique ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * The invoice's linked shippingOrder.
     */
    @OneToOne
    @JoinColumn(name = "shipping_order_id")
    private ShippingOrder shippingOrder;

    /**
     * The invoice's payer type.
     */
    @ManyToOne
    @JoinColumn(name = "payer_type_id") // invoice_payer_type_id was a mismatch in the DB
    private InvoicePayerType invoicePayerType;

    /**
     * The invoice's third party.
     */
    @ManyToOne
    @JoinColumn(name = "third_party_id")
    private ThirdParty thirdParty;

    /**
     * The invoice's customer.
     */
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    /**
     * Date invoice is created
     */
    private Date created_at;

    /**
     * Due date of the invoice payment
     */
    private Date due_date;

    /**
     * Status of the invoice payment
     */
    private boolean is_paid;

    /**
     * No args constructor.
     */
    public Invoice() {

    }

    /**
     * Constructs an instance of this class
     *
     * @param id               the invoice id.
     * @param shippingOrder    shipping order related to the invoice.
     * @param invoicePayerType the payer type for the given invoice.
     * @param thirdParty       the third party related to the invoice.
     * @param customer         customer for the invoice.
     * @throws IllegalArgumentException if some inputs is not correct.
     */
    public Invoice(int id, ShippingOrder shippingOrder, InvoicePayerType invoicePayerType, ThirdParty thirdParty, Customer customer, Date created_at, Date due_date, boolean is_paid) {
        if (InputChecker.isValid(id) &&
                shippingOrder != null &&
                invoicePayerType != null &&
                thirdParty != null &&
                customer != null) {

            this.id = id;
            this.shippingOrder = shippingOrder;
            this.invoicePayerType = invoicePayerType;
            this.thirdParty = thirdParty;
            this.customer = customer;
            this.created_at = created_at;
            this.due_date = due_date;
            this.is_paid = is_paid;
        } else {
            throw new IllegalArgumentException("Invalid input");
        }
    }

    /**
     * Returns the invoice's ID.
     *
     * @return the invoice's ID.
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the invoice's related shipping order id.
     *
     * @return the invoice's related shipping order id.
     */
    public ShippingOrder getShippingOrderId() {
        return shippingOrder;
    }

    /**
     * Returns the invoice's related payer type.
     *
     * @return the invoice's related payer type.
     */
    public InvoicePayerType getInvoicePayerType() {
        return invoicePayerType;
    }

    /**
     * Returns the invoice's third party.
     *
     * @return the invoice's third party.
     */
    public ThirdParty getThirdParty() {
        return this.thirdParty;
    }

    /**
     * Returns the invoice's customer.
     *
     * @return the invoice's customer.
     */
    public Customer getCustomer() {
        return this.customer;
    }

    /**
     * Returns the date invoice is created
     *
     * @return created date
     */
    public Date getCreated_at() {
        return created_at;
    }

    /**
     * Returns the date of the invoice due date
     *
     * @return due date of the invoice
     */
    public Date getDue_date() {
        return due_date;
    }

    /**
     * Returns the status of the invoice payment
     *
     * @return invoice payment statement
     */
    public boolean isIs_paid() {
        return is_paid;
    }
}
