package no.ntnu.queryeng.entity;

import jakarta.persistence.*;
import no.ntnu.queryeng.tools.InputChecker;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a shipping order -object in the query engine application.
 *
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 01.03.2023
 */
@Entity
public class ShippingOrder {

    /***
     * The ID of the shipping order.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    /**
     * The ID of the sender of the shipping order.
     */
    @ManyToOne
    @JoinColumn(name = "sender_id", insertable = false, updatable = false)
    private Customer sender;

    /**
     * The ID of the receiver of the shipping order.
     */
    @ManyToOne
    @JoinColumn(name = "receiver_id", insertable = false, updatable = false)
    private Customer receiver;

    /**
     * The date and time for the shipping order.
     */
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    /**
     * List of parcels connected to this shipping order
     */
    @OneToMany(mappedBy = "shippingOrder")
    private List<Parcel> parcelList;

    /**
     * Initialises the ShippingOrder-object.
     *
     * @param id        the id of the shippingOrder
     * @param createdAt the date and time
     * @throws IllegalArgumentException if some inputs is not correct.
     */
    public ShippingOrder(int id, LocalDateTime createdAt) {
        if (InputChecker.isValid(id) && createdAt != null) {
            this.id = id;
            this.createdAt = createdAt.now();
            this.parcelList = new ArrayList<>();
        } else {
            throw new IllegalArgumentException("Invalid input");
        }
    }

    /**
     * No args constructor. Intentionally left empty.
     */
    public ShippingOrder() {
    }

    /**
     * Returns the shippingOrder ID.
     * @return the shippingOrder ID.
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the sender of the shipping order.
     * @return the sender of the shipping order.
     */
    public Customer getSender() {
        return sender;
    }

    /**
     * Returns the reciever of the shipping order.
     * @return the reciever of the shipping order.
     */
    public Customer getReceiver() {
        return receiver;
    }

    /**
     * Returns the date and time the shippingOrder was created at.
     * @return the date and time the shippingOrder was created at.
     */
    public LocalDateTime getDateAndTime() {
        return createdAt;
    }

    /**
     * Returns the list of parcels in the shipping order.
     * @return the list of parcels in the shipping order
     */
    public List<Parcel> getParcelList() {
        return this.parcelList;
    }
}
