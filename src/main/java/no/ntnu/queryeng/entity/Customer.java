package no.ntnu.queryeng.entity;

import jakarta.persistence.*;
import no.ntnu.queryeng.tools.InputChecker;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a customer table in the database.
 */
@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "address")
    private String address;

    @Column(name = "post_code")
    private String postalCode;

    @Column(name = "city")
    private String city;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "email")
    private String email;

    // The list of shipping orders linked to this customer.
    @OneToMany(mappedBy = "sender")
    private List<ShippingOrder> senderList;

    // The list of shipping orders linked to this customer.
    @OneToMany(mappedBy = "receiver")
    private List<ShippingOrder> receiverList;

    // The list of the invoices mapped to this customer.
    @OneToMany(mappedBy = "customer")
    private List<Invoice> invoices;

    // The list of the additional service invoices mapped to this customer.
    @OneToMany(mappedBy = "customer")
    private List<AdditionalServiceInvoice> additionalServiceInvoices;

    /**
     * Creates an instance of a customer.
     *
     * @param id         the customer's id.
     * @param firstName  the customer's first name.
     * @param lastName   the customer's last name.
     * @param address    the customer's address.
     * @param postalCode the customer's postal code.
     * @param city       the customer's city.
     * @param mobile     the customer's mobile number.
     * @param email      the customer's email address.
     * @throws IllegalArgumentException if some inputs is not correct.
     */
    public Customer(int id, String firstName, String lastName, String address, String postalCode, String city,
            String mobile, String email) {
        if (InputChecker.isValid(id) &&
                InputChecker.isValid(firstName) &&
                InputChecker.isValid(lastName) &&
                InputChecker.isValid(address) &&
                InputChecker.isValid(postalCode) &&
                InputChecker.isValid(city) &&
                InputChecker.isValid(mobile) &&
                InputChecker.isValid(email)) {

            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.address = address;
            this.postalCode = postalCode;
            this.city = city;
            this.mobile = mobile;
            this.email = email;
            this.senderList = new ArrayList<>();
            this.receiverList = new ArrayList<>();
            this.invoices = new ArrayList<>();
            this.additionalServiceInvoices = new ArrayList<>();
        } else {
            throw new IllegalArgumentException("Invalid input");
        }
    }

    /**
     * No args constructor. Intentionally left empty.
     */
    public Customer() {

    }

    /**
     * Returns the customers ID.
     * 
     * @return the customers ID.
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the customers first name.
     * 
     * @return the customers first name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Returns the customers last name.
     * 
     * @return the customers last name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Returns the customers address.
     * 
     * @return the customers address.
     */
    public String getAddress() {
        return address;
    }

    /**
     * Returns the customers postal code.
     * 
     * @return the customers postal code.
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Returns the customers city.
     * 
     * @return the customers city.
     */
    public String getCity() {
        return city;
    }

    /**
     * Returns the customers mobile number.
     * 
     * @return the customers mobile number.
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * Returns the customers email address.
     * 
     * @return the customers email address.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Returns the customers list of shipment that is either sent or under
     * transport.
     * 
     * @return the customers list of shipment that is either sent or under
     *         transport.
     */
    public List<ShippingOrder> getSenderList() {
        return senderList;
    }

    /**
     * Returns the customers list of shipment that is either received or under
     * transport.
     * 
     * @return the customers list of shipment that is either received or under
     *         transport.
     */
    public List<ShippingOrder> getReceiverList() {
        return receiverList;
    }

    /**
     * Returns the customers invoice.
     * 
     * @return the customers invoice.
     */
    public List<Invoice> getInvoices() {
        return invoices;
    }

    /**
     * Returns the customers additional service invoices.
     * 
     * @return the customers additional service invoices.
     */
    public List<AdditionalServiceInvoice> getAdditionalServiceInvoices() {
        return additionalServiceInvoices;
    }
}
