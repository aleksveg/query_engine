package no.ntnu.queryeng.entity;

import jakarta.persistence.*;
import no.ntnu.queryeng.tools.InputChecker;


/**
 * Class represents additional service that can be added to a shipping order.
 *
 * @author aleksander & Joakim Røren Melum
 * @version 0.1
 * @date 25.04.2023
 */

@Entity
@Table(name = "additional_service")
public class AdditionalService {

    /**
     * The unique id of the additional service.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    /**
     * The additional service name of the additional service.
     */
    @Column(name = "additional_service")
    private String additionalServiceName;

    /**
     * The description of the additional service.
     */
    @Column(name = "description")
    private String description;

    /**
     * The cost of the additional service.
     */
    @Column(name = "cost")
    private double cost;

    /**
     * The additional service order connected to the additional service.
     */
    @OneToOne
    @JoinColumn(name = "additional_service_order_id") //TODO: FIX THIS!
    private AdditionalServiceOrder additionalServiceOrder;

    /**
     * No argument constructor for additional service. Intentionally left empty.
     */
    public AdditionalService() {

    }

    /**
     * Constructs an instance of this class
     *
     * @param additionalServiceName the name of the additional service
     * @param description           description of additional service
     * @param cost                  cost of the additional service
     * @throws IllegalArgumentException if some inputs is not correct.
     */
    public AdditionalService(int id, String additionalServiceName, String description, double cost) {
        if (InputChecker.isValid(additionalServiceName) && InputChecker.isValid(description) && InputChecker.isValid(cost) && InputChecker.isValid(id)) {
            this.id = id;
            this.additionalServiceName = additionalServiceName;
            this.description = description;
            this.cost = cost;
        } else {
            throw new IllegalArgumentException("Invalid input");
        }
    }

    /**
     * Return the additional service's id.
     * @return the additional service's id.
     */
    public int getId() {
        return id;
    }

    /**
     * Return the additional service's name.
     * @return the additional service's name.
     */
    public String getAdditionalServiceName() {
        return additionalServiceName;
    }

    /**
     * Return the additional service's description.
     * @return the additional service's description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Return the additional service's cost.
     * @return the additional service's cost.
     */
    public double getCost() {
        return cost;
    }

    /**
     * Return the additional service order connected to the additional service.
     * @return the additional service order connected to the additional service.
     */
    public AdditionalServiceOrder getAdditionalServiceOrder() {
        return this.additionalServiceOrder;
    }
}
