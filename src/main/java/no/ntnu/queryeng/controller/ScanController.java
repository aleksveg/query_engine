package no.ntnu.queryeng.controller;

import no.ntnu.queryeng.entity.Scan;
import no.ntnu.queryeng.entity.Terminal;
import no.ntnu.queryeng.repository.ScanRepository;
import no.ntnu.queryeng.repository.TerminalRepo;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.sql.Date;
import java.util.List;

@Controller
public class ScanController {
    // creates a instance of scanRepository as a variable
    private ScanRepository scanRepository;

    /**
     * Creates an instance and Initiates the ScanController class
     * @param scanRepository //ToDo: Define
     */
    public ScanController(ScanRepository scanRepository) {
        this.scanRepository = scanRepository;
    }

    /**
     * A query for fetching a scan by its ID
     * @param id the id to search for
     * @return the scan with the given id
     */
    @QueryMapping
    public Scan scanById(@Argument int id) {
        return scanRepository.getById(id);
    }

    /**
     * A Query for fetching a list of scans with a given status message
     * Examples: status_id indexed by number 1 = "ARRIVED"
     * Examples: status_id indexed by number 2 = "PICKED"
     * Examples: status_id indexed by number 3 = "UNDER TRANSPORT"
     *
     * @return returns a list of scans with a status message filtered by the status messages id
     */
    @QueryMapping
    public List<Scan> fetchAllScans() {return scanRepository.getAllScans();}

    @QueryMapping
    public List<Scan> fetchScansByYear(@Argument String year){
        return scanRepository.getAllScansByYear(year);
    }

    @QueryMapping
    public List<Scan> fetchScansByDateRange(@Argument Date startDate, @Argument Date endDate){
        return  scanRepository.getAllScansByDateLimit(startDate, endDate);
    }

    @QueryMapping
    public int fetchScansCountByDateRange(@Argument Date startDate, @Argument Date endDate){
        return scanRepository.getAllScansCountByDateLimit(startDate, endDate);
    }
}