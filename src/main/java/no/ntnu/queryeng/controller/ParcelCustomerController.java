package no.ntnu.queryeng.controller;

import no.ntnu.queryeng.queryEntity.ParcelCustomer;
import no.ntnu.queryeng.repository.ParcelCustomerRepo;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ParcelCustomerController {

    public ParcelCustomerController(ParcelCustomerRepo parcelCustomerRepo) {
        this.parcelCustomerRepo = parcelCustomerRepo;
    }

    private ParcelCustomerRepo parcelCustomerRepo;
    /**
     * Total customers grouped according to number of parcels sent.
     *
     * @return total customers sort after number of parcels
     */
    @QueryMapping
    List<ParcelCustomer> getTotalCustomersPerParcel() {
        return parcelCustomerRepo.findCustomersCountPerParcel();
    }
}
