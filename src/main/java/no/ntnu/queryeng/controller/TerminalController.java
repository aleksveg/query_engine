package no.ntnu.queryeng.controller;

import no.ntnu.queryeng.entity.Terminal;
import no.ntnu.queryeng.repository.TerminalRepo;
import no.ntnu.queryeng.service.TerminalService;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;
import java.util.List;

/**
 * This class takes use of GraphQL for making queries using the notation: @QueryMapping and @SchemaMapping
 * //ToDo: Fill in a more complete Description of the class
 *
 * @author odin
 * @version 1.1.0
 */

@Controller
public class TerminalController {

    private TerminalRepo terminalRepo;

    public TerminalController(TerminalRepo terminalRepo) {
        this.terminalRepo = terminalRepo;
    }

    /**
     * Fetches the "type query" defined in resoruces/graphql/schema.graphqls in order to fetch the Terminal
     * of a given id (an int), if found it returns the terminal with the id matching int requested.
     * Example: input 1, output (1, "Orsta", "Haakonsgata 23", 50)
     *
     * @param id the integer value of the terminal you wish to search for
     * @return the terminal with the matching id
     */
    @QueryMapping
    public Terminal terminalById(@Argument int id) {
        return terminalRepo.getById(id);
    }

    @QueryMapping
    public Terminal terminalByName(@Argument String name) {
        return terminalRepo.findTerminalByName(name);
    }

    @QueryMapping
    public List<Terminal> fetchAllTerminals(){ return (List<Terminal>) terminalRepo.findAll();
    }
}
