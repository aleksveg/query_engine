package no.ntnu.queryeng.controller;

import no.ntnu.queryeng.repository.TerminalScanCostRepository;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

@Controller
public class TerminalScanCostController {

    private TerminalScanCostRepository terminalScanCostRepository;

    public TerminalScanCostController(TerminalScanCostRepository terminalScanCostRepository) {
        this.terminalScanCostRepository = terminalScanCostRepository;
    }

    /**
     * Returns the total cost of all scans for all terminals
     * @return total costs of all scans for all terminals
     */
    @QueryMapping
    Integer getScanCostForAllTerminals() {
        return this.terminalScanCostRepository.getTotalCostOfScans();
    }

}
