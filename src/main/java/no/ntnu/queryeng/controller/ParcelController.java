package no.ntnu.queryeng.controller;

import no.ntnu.queryeng.entity.Parcel;
import no.ntnu.queryeng.repository.ParcelRepository;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;
import java.util.List;

/**
 * This class takes use of GraphQL for making queries using the notation: @QueryMapping and @SchemaMapping
 */
@Controller
public class ParcelController {
    private ParcelRepository parcelRepository;

    public ParcelController(ParcelRepository parcelRepository) {
        this.parcelRepository = parcelRepository;
    }

    @QueryMapping
    public Parcel parcelById(@Argument long id) {
        return parcelRepository.getById(id);
    }

    @QueryMapping
    public List<Parcel> parcelsByGivenStatus(@Argument int statusId) {
        return parcelRepository.getParcelsByStatusCode(statusId);
    }

    @QueryMapping
    public int getSumShippingCostOfParcel() {
        return parcelRepository.getSumShippingCostOfParcel();
    }
}
