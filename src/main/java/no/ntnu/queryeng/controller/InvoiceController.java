package no.ntnu.queryeng.controller;

import no.ntnu.queryeng.entity.Invoice;
import no.ntnu.queryeng.repository.InvoiceRepo;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.sql.Date;
import java.util.List;

@Controller
public class InvoiceController {

    private InvoiceRepo invoiceRepo;

    public  InvoiceController (InvoiceRepo invoiceRepo) { this.invoiceRepo = invoiceRepo; }

    // Default test query to figure out if the GraphQL queries work as intended
    @QueryMapping
    public Invoice getInvoiceById(@Argument int id) { return invoiceRepo.getInvoiceById(id); }

    @QueryMapping
    public List<Invoice> getAllInvoices () { return invoiceRepo.findAllByLimit();}

    /**
     * Fetches the amount of invoices as a number
     * @return the amount of invoices as an integer
     */
    @QueryMapping
    public Integer getNumberOfInvoices () { return invoiceRepo.getNumberOfInvoices(); }

    @QueryMapping
    public Integer getSumOfParcels() {
        return invoiceRepo.getSumShippingCostOfParcel();
    }

    /**
     * Returns all the invoices between given date range
     * @param startDate start date to search from
     * @param endDate last date to search to
     * @return list of invoices
     */
    @QueryMapping
    public List<Invoice> getInvoiceByDateRange(@Argument Date startDate, @Argument Date endDate){
        return invoiceRepo.getInvoiceByDateRange(startDate, endDate);
    }

    /**
     * Returns total unpaid amount from the invoices
     * @return unpoid amount
     */
    @QueryMapping
    public int getUnPaidAmount(){
        return invoiceRepo.getTotalUnpaidAmount();
    }

}
