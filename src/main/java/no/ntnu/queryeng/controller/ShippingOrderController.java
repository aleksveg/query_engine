package no.ntnu.queryeng.controller;

import no.ntnu.queryeng.entity.Scan;
import no.ntnu.queryeng.entity.ShippingOrder;
import no.ntnu.queryeng.entity.Terminal;
import no.ntnu.queryeng.repository.ShippingOrderRepository;
import no.ntnu.queryeng.service.ShippingOrderService;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * This class takes use of GraphQL for making queries using the notation: @QueryMapping and @SchemaMapping
 * //ToDo: Fill in a more complete Description of the class
 * //TODO: borrowed some of odins documentation until we know
 * //TODO: which methods and classes that are supposed to be used. will replace documentation later.
 *
 * @author Joakim
 * @version 0.0.1
 */

@Controller
public class ShippingOrderController {
    /**
     * Fetches the "type query" defined in resoruces/graphql/schema.graphqls in order to fetch the ShippingOrder
     * of a given id (an int), if found it returns the ShippingOrder with the id matching the id requested.
     * Example: input 1, output (1, "Arrived")
     *
     * @param id the integer value of the ShippingOrder you wish to search for
     * @return the ShippingOrder with the matching id
     *
    @QueryMapping
    public ShippingOrderService ShippingOrderById(@Argument int id) {
        return ShippingOrderService.getById(id);
    } */

    private ShippingOrderRepository shippingOrderRepository;
    public ShippingOrderController (ShippingOrderRepository shippingOrderRepository) { this.shippingOrderRepository = shippingOrderRepository; }

    @QueryMapping
    public ShippingOrder shippingOrderById (@Argument int id) {
        return shippingOrderRepository.getById(id);
    }

    @QueryMapping
    public List<ShippingOrder> fetchAllShippingOrders(){
        return (List<ShippingOrder>) shippingOrderRepository.findAll();
    }
}
