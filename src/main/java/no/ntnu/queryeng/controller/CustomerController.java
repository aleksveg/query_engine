package no.ntnu.queryeng.controller;

import no.ntnu.queryeng.entity.Customer;
import no.ntnu.queryeng.queryEntity.CityWithCustomers;
import no.ntnu.queryeng.queryEntity.PostCodeCustomer;
import no.ntnu.queryeng.repository.CustomerRepo;
import no.ntnu.queryeng.repository.PostCodeCustomerRepo;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * Represents customer controller with queries to fetch required information.
 */
@Controller
public class CustomerController {
    private CustomerRepo customerRepo;
    private PostCodeCustomerRepo postCodeCustomerRepo;

    public CustomerController(CustomerRepo customerRepo, PostCodeCustomerRepo postCodeCustomerRepo) {
        this.customerRepo = customerRepo;
        this.postCodeCustomerRepo = postCodeCustomerRepo;
    }



    /**
     * Query to find all customers in the database
     * 
     * @return list of all customers
     */
    @QueryMapping
    List<Customer> getAllCustomers() {
        return customerRepo.findAllByLimit();
    }

    /**
     * Query to find customers matching the following parameters
     * 
     * @param fname  first name of the customer
     * @param lname  last name of the customer
     * @param pCity  city of the customer
     * @param mob mobile number of the customer
     * @param mail  email address of the customer
     * @param pCode  post code of the customer
     * @return list of customers matching the parameters
     */
    @QueryMapping
    List<Customer> getCustomersByName(@Argument String fname,
            @Argument String lname,
            @Argument String pCode,
            @Argument String pCity,
            @Argument String mob,
            @Argument String mail) {
        return customerRepo.findAllByName(fname, lname, pCity, mob, mail, pCode);
    }

    /**
     * Get the largest post code in the database.
     * 
     * @return the largest post code object with the largest post code and the
     *         count.
     */
    @QueryMapping
    PostCodeCustomer getLargestPostCode() {
       return postCodeCustomerRepo.findLargestPostCode();
    }

    /**
     * Get the smallest post code in the database.
     *
     * @return the smallest post code object with the smallest post code and the
     *         count.
     */
    @QueryMapping
    PostCodeCustomer getSmallestPostCode() {
        return postCodeCustomerRepo.findSmallestPostCode();
    }

    /**
     * Get all post codes in the database.
     * 
     * @return all post codes in the database
     */
    @QueryMapping
    List<PostCodeCustomer> getAllPostCodes() {
        return postCodeCustomerRepo.findAllPostCode();
    }

    @QueryMapping
    List<CityWithCustomers> getAllCitiesWithCustomers() {
        return customerRepo.findAllCitiesWithCustomers();
    }

    /**
     * Total customers registered in the database.
     * 
     * @return total customers registered in the database
     */
    @QueryMapping
    int getTotalCustomers() {
        return customerRepo.countCustomers();
    }
}
