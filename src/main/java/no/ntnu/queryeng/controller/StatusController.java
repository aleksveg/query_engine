package no.ntnu.queryeng.controller;

import no.ntnu.queryeng.entity.Status;
import no.ntnu.queryeng.entity.Terminal;
import no.ntnu.queryeng.repository.StatusRepo;
import no.ntnu.queryeng.repository.TerminalRepo;
import no.ntnu.queryeng.service.StatusService;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * This class takes use of GraphQL for making queries using the notation: @QueryMapping and @SchemaMapping
 * //ToDo: Fill in a more complete Description of the class
 *
 * @author odin
 * @version 1.0.0
 */

@Controller
public class StatusController {

    private StatusRepo statusRepo;

    public StatusController(StatusRepo statusRepo) {
        this.statusRepo = statusRepo;
    }
    /**
     * Fetches the "type query" defined in resoruces/graphql/schema.graphqls in order to fetch the Status
     * of a given id (an int), if found it returns the status with the id matching int requested.
     * Example: input 1, output (1, "Arrived")
     *
     * @param id the integer value of the status you wish to search for
     * @return the status with the matching id
     */
    @QueryMapping
    public Status statusById(@Argument int id) {
        return statusRepo.getById(id);
    }

    /**
     * Gets all statuses
     * @return returns a list of status
     */
    @QueryMapping
    public List<Status> getAllStatus() {
        return (List<Status>) statusRepo.findAll();
    }
}
