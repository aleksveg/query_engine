package no.ntnu.queryeng.queryEntity;

public class CityWithCustomers {
    private String city;
    private int customerCount;

    public CityWithCustomers(String city, int customerCount) {
        this.city = city;
        this.customerCount = customerCount;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCustomerCount() {
        return customerCount;
    }

    public void setCustomerCount(int customerCount) {
        this.customerCount = customerCount;
    }
}
