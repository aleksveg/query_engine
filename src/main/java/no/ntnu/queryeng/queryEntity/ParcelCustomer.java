package no.ntnu.queryeng.queryEntity;

import jakarta.persistence.*;

/**
 * General class to be used when query returns a category and count.
 */
@Entity
@Table(name = "parcel_customer")
public class ParcelCustomer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "no_of_parcel")
    private int noOfParcel;
    private int customers;

    /**
     * Instance of ParcelCustomer
     * @param noOfParcel category to count
     * @param customers total per category
     */
    public ParcelCustomer(int noOfParcel, int customers) {
        this.noOfParcel = noOfParcel;
        this.customers = customers;
    }

    public ParcelCustomer() {
    }

    /**
     * Returns a category id
     * @return category id
     */
    public int getNoOfParcel() {
        return noOfParcel;
    }

    /**
     * Returns total per category
     * @return total per category
     */
    public int getCustomers() {
        return customers;
    }

}
