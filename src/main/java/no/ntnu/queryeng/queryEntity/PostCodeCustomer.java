package no.ntnu.queryeng.queryEntity;

import jakarta.persistence.*;

@Entity
@Table(name = "postcode_customer")
public class PostCodeCustomer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String postCode;
    private int customers;

    public PostCodeCustomer(String postCode, int customers) {
        this.postCode = postCode;
        this.customers = customers;
    }

    public PostCodeCustomer() {

    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public int getCustomers() {
        return customers;
    }

    public void setCustomers(int customers) {
        this.customers = customers;
    }
}
