package no.ntnu.queryeng.queryEntity;


import jakarta.persistence.*;

@Entity
@Table(name = "terminal_scan_cost")
public class TerminalScanCost {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "terminal_id")
    private int terminalId;

    @Column(name = "scan_cost")
    private int scanCost;

    public TerminalScanCost() {
    }


    public int getTerminalId() {
        return terminalId;
    }

    public int getScanCost() {
        return scanCost;
    }
}
