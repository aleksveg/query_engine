package no.ntnu.queryeng.repository;

import no.ntnu.queryeng.entity.InvoicePayerType;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository class for invoice payer type
 */
public interface InvoicePayerTypeRepository extends CrudRepository<InvoicePayerType, Integer> {
}
