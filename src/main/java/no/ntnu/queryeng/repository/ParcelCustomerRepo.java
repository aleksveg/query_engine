package no.ntnu.queryeng.repository;

import no.ntnu.queryeng.queryEntity.ParcelCustomer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParcelCustomerRepo extends CrudRepository<ParcelCustomer, Integer> {
    /**
     * Total customers grouped according to number of parcels sent.
     *
     * @return total customers sort after number of parcels
     */
    @Query(value = "SELECT * FROM parcel_customer order by customers desc ", nativeQuery = true)
    List<ParcelCustomer> findCustomersCountPerParcel();
}
