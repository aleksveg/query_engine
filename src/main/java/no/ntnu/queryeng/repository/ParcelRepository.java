package no.ntnu.queryeng.repository;

import no.ntnu.queryeng.entity.Parcel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Represents a parcel repository.
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 01.03.2023
 */
@Repository
public interface ParcelRepository extends CrudRepository<Parcel, Long> {

    /**
     * Query for finding a parcel by their given id
     * @param id The id to be searched for
     * @return returns the parcel with matching id
     */
    @Query(value = "SELECT * FROM Parcel WHERE id = ?1", nativeQuery = true)
    Parcel getById(long id);

    // Work in progress, Does not fully work atm asking for a parcel by it's status
    @Query(value = "SELECT * FROM Parcel Scan WHERE status_id = ?1", nativeQuery = true)
    List<Parcel> getParcelsByStatusCode(int statusId);

    @Query(value = "SELECT * FROM Parcel", nativeQuery = true)
    List<Parcel> findAllParcels();

    @Query(value = "SELECT SUM(cost) FROM parcel_cost_view", nativeQuery = true)
    int getSumShippingCostOfParcel();
}
