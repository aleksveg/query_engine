package no.ntnu.queryeng.repository;

import no.ntnu.queryeng.entity.Scan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

/**
 * Repository class for Scan
 */
@Repository
public interface ScanRepository extends CrudRepository<Scan, Integer> {

    /**
     * Query for finding a scan by its id
     *
     * @param id The id to be searched for
     * @return returns the scan with matching id
     */
    @Query(value = "SELECT * FROM Scan WHERE id = ?1", nativeQuery = true)
    Scan getById(int id);

    /**
     * Query for finding a parcel with a given status message
     * Examples: status_id indexed by number 1 = "ARRIVED"
     * Examples: status_id indexed by number 2 = "PICKED"
     * Examples: status_id indexed by number 3 = "UNDER TRANSPORT"
     *
     * @return returns a list of scans with the given search input for status_id
     */
    @Query(value = "SELECT * FROM scan limit 100", nativeQuery = true)
    List<Scan> getAllScans();

    @Query(value = "SELECT * FROM scan WHERE YEAR(scanned_at) = :year limit 100", nativeQuery = true)
    List<Scan> getAllScansByYear(@Param("year") String year);

    @Query(value = "SELECT * FROM scan WHERE scanned_at BETWEEN :startDate AND :endDate limit 100", nativeQuery = true)
    List<Scan> getAllScansByDateLimit(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

    @Query(value = "SELECT count(*) FROM scan WHERE scanned_at BETWEEN :startDate AND :endDate limit 100", nativeQuery = true)
    int getAllScansCountByDateLimit(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

}
