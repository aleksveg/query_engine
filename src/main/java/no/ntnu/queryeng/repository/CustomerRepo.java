package no.ntnu.queryeng.repository;

import no.ntnu.queryeng.entity.Customer;
import no.ntnu.queryeng.queryEntity.CityWithCustomers;
import no.ntnu.queryeng.queryEntity.PostCodeCustomer;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Represents customer repository.
 */
public interface CustomerRepo extends CrudRepository<Customer, Integer> {
    /**
     * Query for finding a customer by their id
     *
     * @param id The id to be searched for
     * @return the customer with matching id
     */
    @Query(value = "SELECT * FROM Customer WHERE id = ?1", nativeQuery = true)
    Customer getById(int id);

    @Query(value = "SELECT * FROM Customer limit 100", nativeQuery = true)
    List<Customer> findAllByLimit();

    /**
     * Query to find customers matching the following parameters
     *
     * @param fname  first name of the customer
     * @param lname  last name of the customer
     * @param pCity  city of the customer
     * @param mobile mobile number of the customer
     * @param email  email address of the customer
     * @param pCode  post code of the customer
     * @return list of customers matching the parameters
     */
    @Query(value = "SELECT * FROM Customer " +
            "WHERE (LOWER(first_name) LIKE '%' || LOWER(?1) || '%') AND" +
            " (LOWER(last_name) LIKE '%' || LOWER(?2) || '%') AND " +
            "(LOWER(city) LIKE '%' || LOWER(?3) || '%') AND " +
            "(LOWER(mobile) LIKE '%' || LOWER(?4) || '%') AND " +
            "(LOWER(email) LIKE '%' || LOWER(?5) || '%') AND " +
            "((?6 is NULL or ?6 = '' ) OR post_code = ?6) LIMIT 100", nativeQuery = true)
    List<Customer> findAllByName(String fname, String lname, String pCity, String mobile, String email,
                                 String pCode);





    @Query("SELECT new no.ntnu.queryeng.queryEntity.CityWithCustomers(c.city, CAST(COUNT(c.id) AS int)) FROM Customer c GROUP BY c.city ORDER BY COUNT(c.id) DESC LIMIT 10 ")
    List<CityWithCustomers> findAllCitiesWithCustomers();

    // Return number of customers.
    @Query(value = "SELECT COUNT(*) FROM Customer", nativeQuery = true)
    int countCustomers();

}
