package no.ntnu.queryeng.repository;

import no.ntnu.queryeng.entity.Status;
import no.ntnu.queryeng.entity.Terminal;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Represents the Status Repository, adapted for GraphQL
 * In the repository classes we'll define queries to be made. this class has a connection to the
 * StatusController class in such a way that controller fetches methods from this interface
 *
 * @author odin
 * @version 1.0.0
 */
public interface StatusRepo extends CrudRepository<Status, Integer>{
    // Left empty intentionally...

    @Query(value = "SELECT * FROM Status WHERE id = ?1", nativeQuery = true)
    Status getById(int id);
}
