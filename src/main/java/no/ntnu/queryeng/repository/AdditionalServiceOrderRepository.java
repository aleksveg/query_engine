package no.ntnu.queryeng.repository;

import no.ntnu.queryeng.entity.AdditionalServiceOrder;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository class for additional service order
 */
public interface AdditionalServiceOrderRepository extends CrudRepository<AdditionalServiceOrder, Integer> {
}
