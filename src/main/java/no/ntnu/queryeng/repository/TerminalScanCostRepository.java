package no.ntnu.queryeng.repository;

import no.ntnu.queryeng.queryEntity.TerminalScanCost;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface TerminalScanCostRepository extends CrudRepository<TerminalScanCost, Integer> {


    /**
     * Returns the total sum of scan costs related to terminals
     * @return total sum of scan costs related to terminals
     */
    @Query(value = "SELECT SUM(SCAN_COST) FROM terminal_scan_cost", nativeQuery = true)
    Integer getTotalCostOfScans();

}
