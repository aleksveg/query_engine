package no.ntnu.queryeng.repository;

import no.ntnu.queryeng.entity.ShippingCost;
import org.springframework.data.repository.CrudRepository;

public interface ShippingCostRepository extends CrudRepository<ShippingCost, Integer> {
}
