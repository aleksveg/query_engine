package no.ntnu.queryeng.repository;

import no.ntnu.queryeng.entity.Status;
import no.ntnu.queryeng.entity.Terminal;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the Terminal Repository, adapted for GraphQL
 * In the repository classes we'll define queries to be made. this class has a connection to the
 * TerminalController class in such a way that controller fetches methods from this interface
 *
 * @author odin
 * @version 1.1.0
 */
@Repository
public interface TerminalRepo extends CrudRepository<Terminal, Integer> {
    // Left empty intentionally...

    /**
     * QUERY fetching the name of a terminal by its name. The syntax
     * 'name = ?1' where the question-mark defines the String to be searched for
     * 1 is the first parameter of the method
     * ? defines what to be searched for
     *
     * @param name of the terminal you wish to search for
     * @return the first terminal with matching name
     */
    @Query(value = "SELECT * FROM Terminal WHERE name = ?1", nativeQuery = true)
    Terminal findTerminalByName(String name);

    /**
     * QUERY fetching the id of a terminal by the terminals id number. The syntax
     * 'id = ?1' where the question mark defines the int id to be searched for.
     * 1 is the first parameter of the method
     * ? defines what to be searched for
     *
     * @param id of the terminal you wish to search for
     * @return the first terminal with matching id
     */
    @Query(value = "SELECT * FROM Terminal WHERE id = ?1", nativeQuery = true)
    Terminal getById(int id);

    @Query(value = "SELECT * FROM terminal ORDER BY id ASC", nativeQuery = true)
    List<Terminal> fetchAllTerminals();
}
