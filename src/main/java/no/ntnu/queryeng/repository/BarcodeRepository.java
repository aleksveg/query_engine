package no.ntnu.queryeng.repository;

import no.ntnu.queryeng.entity.Barcode;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository class for barcode.
 */
public interface BarcodeRepository extends CrudRepository<Barcode, Integer> {
}
