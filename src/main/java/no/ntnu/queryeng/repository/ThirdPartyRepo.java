package no.ntnu.queryeng.repository;

import no.ntnu.queryeng.entity.ThirdParty;
import org.springframework.data.repository.CrudRepository;

/**
 * Represents a third party repo.
 */
public interface ThirdPartyRepo extends CrudRepository<ThirdParty, Integer> {
}
