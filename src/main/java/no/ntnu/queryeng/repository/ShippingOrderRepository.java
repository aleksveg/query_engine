package no.ntnu.queryeng.repository;


import
        no.ntnu.queryeng.entity.ShippingOrder;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Represents the Shipping order Repository
 */
@Repository
public interface ShippingOrderRepository extends CrudRepository<ShippingOrder, Integer> {

    @Query(value = "SELECT * FROM shipping_order WHERE id = ?1", nativeQuery = true)
    ShippingOrder getById(int id);

    @Query(value = "SELECT * FROM shipping_order", nativeQuery = true)
    List<ShippingOrder> fetchAllShippingOrders();

}