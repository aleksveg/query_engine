package no.ntnu.queryeng.repository;

import no.ntnu.queryeng.entity.AdditionalServiceInvoice;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository class for additional service invoice
 */
public interface AdditionalServiceInvoiceRepository extends CrudRepository<AdditionalServiceInvoice, Integer> {
}
