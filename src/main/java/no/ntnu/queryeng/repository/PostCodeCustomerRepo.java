package no.ntnu.queryeng.repository;

import no.ntnu.queryeng.queryEntity.PostCodeCustomer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PostCodeCustomerRepo extends CrudRepository<PostCodeCustomer, Integer> {
    @Query(value = "SELECT * FROM postcode_customer order by customers desc limit 1", nativeQuery = true)
    PostCodeCustomer findLargestPostCode();

    @Query(value = "SELECT * FROM postcode_customer order by customers asc limit 1", nativeQuery = true)
    PostCodeCustomer findSmallestPostCode();

    @Query(value = "SELECT * FROM postcode_customer", nativeQuery = true)
    List<PostCodeCustomer> findAllPostCode();

}
