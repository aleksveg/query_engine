package no.ntnu.queryeng.repository;

import no.ntnu.queryeng.entity.Invoice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;

/**
 * Reposotiry class for invoice.
 *
 * @author aleksander
 * @version 1.0-SNAPSHOT
 */
public interface InvoiceRepo extends CrudRepository<Invoice, Integer> {

    /**
     * Returns an invoice with the given id.
     * @param id invoice id
     * @return invoice with the given id
     */
    @Query(value = "SELECT * FROM Invoice WHERE id = ?1", nativeQuery = true)
    Invoice getInvoiceById(int id);

    /**
     * Returns 100 first invoices
     * @return 100 first invoices.
     */
    @Query(value = "SELECT * FROM Invoice limit 15", nativeQuery = true)
    List<Invoice> findAllByLimit();

    /**
     * Work in progress Count, does not currently function as intended
     * @return
     */
    @Query(value = "SELECT COUNT(*) FROM Invoice,", nativeQuery = true)
    Integer getNumberOfInvoices();


    /**
     * Returns the sum of shipping costs related to parcels
     * @return sum of shipping cost for all parcels
     */
    @Query("SELECT SUM(sc.cost) FROM Parcel p JOIN p.shippingCost sc")
    int getSumShippingCostOfParcel();

    /**
     *  Returns the invoices between the given range of dates
     * @param startDate start date to search from
     * @param endDate last date to search to
     * @return list of invoices
     */
    @Query(value = "SELECT * FROM invoice WHERE created_at BETWEEN :startDate AND :endDate limit 100", nativeQuery = true)
    List<Invoice> getInvoiceByDateRange(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

    /**
     * Returns the amount unpaid from the invoices
     * @return unpaid amount
     */
    @Query("SELECT SUM(sc.cost) " +
            "FROM Parcel p " +
            "JOIN Invoice i ON p.shippingOrder.id = i.shippingOrder.id " +
            "JOIN ShippingCost sc ON p.shippingCost.id = sc.id " +
            "WHERE i.is_paid = false")
    int getTotalUnpaidAmount();


}

