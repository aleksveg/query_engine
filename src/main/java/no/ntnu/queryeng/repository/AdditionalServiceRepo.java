package no.ntnu.queryeng.repository;

import no.ntnu.queryeng.entity.AdditionalService;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository class for additional service
 *
 * @author aleksander
 * @version 1.0-SNAPSHOT
 */

public interface AdditionalServiceRepo extends CrudRepository<AdditionalService, Integer> {
}
