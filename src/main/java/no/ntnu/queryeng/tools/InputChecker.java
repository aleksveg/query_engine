package no.ntnu.queryeng.tools;

/**
 * Class represents an input validity checker.
 * To prevent code duplication, the input checking methods are gathered in the same class.
 */
public class InputChecker {

    /**
     * No args constructor. Intentionally left empty.
     */
    public InputChecker() {
    }


    /**
     * Checks if the text supplied is not null and
     * not empty, ignoring any leading or trailing whitespaces.
     *
     * @param input the text to be checked for validity.
     * @return <code>TRUE</code> if the text is valid, and
     * <code>FALSE</code> if the text is invalid.
     */
    public static boolean isValid(String input) {
        return input != null && !input.trim().isEmpty();
    }

    /**
     * Checks if the text is valid.
     *
     * @param input the text to check for validity.
     * @throws IllegalArgumentException if the text is not valid.
     */
    public static void checkValid(String input) {
        if (input == null || input.trim().isEmpty() || input.trim().isBlank()) {
            throw new IllegalArgumentException("string cant be empty or null!");
        }
    }

    /**
     * Checks if the text is valid.
     *
     * @param input the text to check for validity.
     * @param error to be displayed if the text is invalid.
     * @throws IllegalArgumentException if the text is not valid.
     */
    public static void checkValid(String input, String error) {
        if (input == null || input.trim().isEmpty()) {
            throw new IllegalArgumentException(error);
        }
    }

    /**
     * Check if the number is between the specific range.
     *
     * @param input         the number to be checked for validity.
     * @param lowestNumber  is the lowest value the number can have.
     * @param highestNumber is the highest value the number can have.
     * @param error         to be displayed in the exception.
     * @throws IllegalArgumentException if the number is between the specific range.
     */
    public static void checkValid(int input, int lowestNumber, int highestNumber, String error) {
        if ((input >= lowestNumber || input <= highestNumber)) {
            throw new IllegalArgumentException(error);
        }
    }

    /**
     * Check if the number is between the specific range.
     *
     * @param input         the number to be checked for validity.
     * @param lowestNumber  the lowest valid value the number can have.
     * @param highestNumber the highest valid value the number can have.
     * @return <code>TRUE</code> if the number is valid and <code>FALSE</code> if the number is invalid.
     */
    public static boolean isValid(int input, int lowestNumber, int highestNumber) {
        return (input >= lowestNumber && input <= highestNumber);
    }

    /**
     * Returns if the input is valid and the value is over 0.
     *
     * @param input the number to be checked.
     * @return <code>TRUE</code> if the input is over 0, and <code>FALSE</code> if it is below 0.
     */
    public static boolean isValid(int input) {
        return input >= 0;
    }

    /**
     * Returns if the input is valid and the value is over 0.
     *
     * @param input the number to be checked.
     * @return <code>TRUE</code> if the input is over 0, and <code>FALSE</code> if it is below 0.
     */
    public static boolean isValid(double input) {
        return input >= 0;
    }


    /**
     * Returns if the input is valid and the value
     * is between the lowest amount and the highest amount valid.
     *
     * @param input         the number to be checked.
     * @param lowestAmount  the lowest valid value.
     * @param highestAMount the highest valid value.
     * @return <code>TRUE</code> if the input is over 0, and <code>FALSE</code> if it is below 0.
     */
    public static boolean isValid(double input, double lowestAmount, double highestAMount) {
        return (input >= lowestAmount && input <= highestAMount);
    }

    /**
     * Returns if the input is valid and the value is over or equal to 0.
     *
     * @param input the number to be checked.
     * @return <code>TRUE</code> if the input is over or equal to 0, and <code>FALSE</code> if it is below 0.
     */
    public static boolean isValid(Long input) {
        return input >= 0L;
    }
}