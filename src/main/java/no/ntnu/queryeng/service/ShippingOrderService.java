package no.ntnu.queryeng.service;

import java.util.Arrays;
import java.util.List;

/**
 * Represents a service for the shipping order.
 * Temporaly used Odins documentation for div methods, classes and similar.
 * If this class will be used as intended, the documentation will be replaced.
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 01.03.2023
 */

public record ShippingOrderService(long shippingOrder_id, long status_id, long senders_id, long receivers_id, String dateTime, long originalShippingOrderId) {

    // Example on GraphQL:
    // https://www.baeldung.com/java-call-graphql-service
    // Code fetched from:
    // https://spring.io/guides/gs/graphql-server/

    /**
     * Generates an Arraylist filled with predetermined dummy data
     */
    private static List<ShippingOrderService> shippingOrders = Arrays.asList(
            new ShippingOrderService(1, 1, 1,1,"02.03.2023", 1),
            new ShippingOrderService(2, 2, 2,2,"02.03.2023", 2)
    );

    /**
     * Business logic for fetching the id of a shippingOrder using a lambda stream
     *
     * @param shippingOrder_id is the unique int value for the ShippingOrder
     * @return if <code>TRUE</code> it returns the list of ShippingOrders with the matching id
     *         else <code>FALSE</code> it returns null.
     */
    public static ShippingOrderService getById(int shippingOrder_id) {
        return shippingOrders.stream()
                .filter(shippingOrders -> shippingOrders.shippingOrder_id() == shippingOrders.shippingOrder_id)
                .findFirst()
                .orElse(null);
    }
}
