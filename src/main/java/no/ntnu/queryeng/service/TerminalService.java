package no.ntnu.queryeng.service;

import java.util.Arrays;
import java.util.List;

/**
 * Represents a service to be used for the Terminal. Current Service is read only and does
 * not provide adding new terminals into the repository.
 *
 * //ToDo: Class is redundant for the moment until further developing for the project
 *
 * @author odin
 * @version 1.0.0
 */

public record TerminalService(int id, String name, String address, int cost) {

    // Example on GraphQL:
    // https://www.baeldung.com/java-call-graphql-service
    // Code fetched from:
    // https://spring.io/guides/gs/graphql-server/

}


