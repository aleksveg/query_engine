package no.ntnu.queryeng.service;

import java.util.Arrays;
import java.util.List;

/**
 * Represents a service for the parcel.
 *
 *  * //ToDo: Fill in a more complete Description of the class
 *  * //TODO: Currently used some of odins documentation.
 *  * //TODO: As soon as its sure what we are going to use of it,
 *  * //TODO: the documentation will be replaced
 *
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 01.03.2023
 */
public record ParcelService(long id, double weight, int status, long trackingNumber) {



    // Example on GraphQL:
    // https://www.baeldung.com/java-call-graphql-service
    // Code fetched from:
    // https://spring.io/guides/gs/graphql-server/

    /**
     * Generates a Arraylist filled with predetermined dummy data
     */
    private static List<ParcelService> parcels = Arrays.asList(
            new ParcelService(1, 1.1, 1, 12345678),
            new ParcelService(2, 2.2, 2, 2345678)
    );

    /**
     * Business logic for fetching the id of a parcel using a lambda stream
     *
     * @param id is the unique int value for the parcel
     * @return if <code>TRUE</code> it returns the list of parcels with the matching id
     *         else <code>FALSE</code> it returns null.
     */
    public static ParcelService getById(int id) {
        return parcels.stream()
                .filter(parcels -> parcels.id() == parcels.id)
                .findFirst()
                .orElse(null);
    }

}


























//
//
//
//
//    /**
//     * @return all the parcels stored in the register.
//     */
//    public List<Parcel> getAllParcels(){
//        List<Parcel> parcels = new ArrayList<>();
//        parcelRepository.findAll().forEach(parcels::add);
//        return new ArrayList<>((Collection) parcelRepository.findAll());
//    }
//
//
//    /**
//     * Return parcel with specific ID.
//     * @param id of the parcel to return.
//     * @return parcel with specific ID.
//     */
//    public Parcel getParcel(long id) {
//        return parcelRepository.findById(id).get();
//    }
//
//    /**
//     * Add a parcel to the register.
//     * @param parcel to be added to the register.
//     */
//    public void addParcel(Parcel parcel) {
//        parcelRepository.save(parcel);
//    }
//
//    /**
//     * Update the information of a specific parcel.
//     * @param id of the parcel to be updated.
//     * @param parcel to be updated.
//     */
//    public void updateParcel(long id, Parcel parcel) {
//        parcelRepository.save(parcel);
//    }
//
//    /**
//     * Delete a specific parcel based on ID.
//     * @param id of the parcel to be deleted.
//     */
//    public void deleteParcel(long id) {
//        parcelRepository.deleteById(id);
//    }