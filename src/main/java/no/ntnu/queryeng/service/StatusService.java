package no.ntnu.queryeng.service;

import java.util.Arrays;
import java.util.List;

/**
 * Represents a service to be used for the Status. Current Service is read only and does
 * not provide adding new statuses into the repository.
 *
 * //ToDo: Class is redundant for the moment until further developing for the project
 *
 * @author odin
 * @version 1.0.0
 */

public record StatusService(int id, String status) {

    // Example on GraphQL:
    // https://www.baeldung.com/java-call-graphql-service
    // Code fetched from:
    // https://spring.io/guides/gs/graphql-server/

}
