package no.ntnu.queryeng.dummydata;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Random;
import com.github.javafaker.Faker;

/**
 * Populates shipping order and invoice test data to use in PostNord database.
 */
public class ShippingOrderDummyData {
    // Initializing constants
    private static final int NUM_ORDERS = 750000;
    private static final int MIN_CUSTOMER_ID = 1;
    private static final int MAX_CUSTOMER_ID = 500000;
    private static final double PAID_PROBABILITY = 0.7;
    private static final int SENDER = 1;
    private static final int RECEIVER = 2;
    private static final int THIRD_PARTY = 3;

    public static void main(String[] args) throws IOException {
        Random rand = new Random();
        Faker faker = new Faker();

        // Creating files and headers to write test data.
        FileWriter shippingOrderWriter = new FileWriter("shipping_orders.csv");
        shippingOrderWriter.write("id,sender_id,receiver_id,created_at\n");
        FileWriter invoiceWriter = new FileWriter("invoices.csv");
        invoiceWriter.write("id,shipping_order_id,customer_id,third_party_id,payer_type_id,created_at,due_date,is_paid\n");

        // Populates given number of invoices and shipping order.
        for (int i = 1; i <= NUM_ORDERS; i++) {
            int orderId = i;
            // Populating sender id withing the range of customer data.
            int senderId = rand.nextInt(MAX_CUSTOMER_ID - MIN_CUSTOMER_ID + 1) + MIN_CUSTOMER_ID;
            int receiverId;
            // Ensures that sender id and receiver id is not same.
            do {
                receiverId = rand.nextInt(MAX_CUSTOMER_ID - MIN_CUSTOMER_ID + 1) + MIN_CUSTOMER_ID;
            } while (receiverId == senderId);

            // Populates dates between given interval.
            LocalDate shippingOrderDate = faker.date().between(Date.valueOf("2022-04-25"), Date.valueOf("2023-04-25")).toInstant().atZone(ZoneId.of(faker.address().timeZone())).toLocalDate();
            String strShippingOrderDate = shippingOrderDate.toString();

            // Writing data to file
            shippingOrderWriter.write(orderId + "," + senderId + "," + receiverId + "," + strShippingOrderDate + "\n");

            // Setting due date to be 3 weeks after created_at date.
            LocalDate dueDate = shippingOrderDate.plusWeeks(3);

            // Setting default values to use when the value is empty
            String customerId = "0";
            String thirdParty = "0";
            int payerType;


            double selectionChance = rand.nextDouble();
            if (selectionChance < 0.35) {
                customerId = String.valueOf(senderId);
                payerType = SENDER;
            } else if (selectionChance < 0.7) {
                customerId = String.valueOf(receiverId);
                payerType = RECEIVER;
            } else {
                // Sets a random value between 1 to 10, as PostNord has 10 third party entries.
                thirdParty = String.valueOf(rand.nextInt(10) + 1);
                payerType = THIRD_PARTY;
            }

            // Ensures that 70% of invoices are set to true.
            boolean isPaid = rand.nextDouble() < PAID_PROBABILITY;
            invoiceWriter.write(orderId + ","  + orderId + "," + customerId + "," + thirdParty + "," + payerType + "," + shippingOrderDate + "," + dueDate + "," + isPaid + "\n");

        }
        shippingOrderWriter.close();
        invoiceWriter.close();
        System.out.println("Files are created successfully!");
    }
}
