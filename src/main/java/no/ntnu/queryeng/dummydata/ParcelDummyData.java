package no.ntnu.queryeng.dummydata;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Populating dummy parcel data to use in parcel table in PostNord database
 */
public class ParcelDummyData {

    public static void main(String[] args) {
        Random random = new Random();
        try {
            FileWriter writer = new FileWriter("parcel.csv");
            writer.write("id,weight,shipping_order_id,shipping_cost_id,barcode_id\n");

            Set<Integer> usedBarcodeIds = new HashSet<>();
            // a Map to keep track of the number of times each shipping order ID has been used
            Map<Integer, Integer> shippingOrderCounts = new HashMap<>();

            // Generating 750000 dummy data
            for (int i = 1; i <= 750000; i++) {
                int weight;
                int weightCategory = random.nextInt(100);
                if (weightCategory < 90) {
                    // 90% of the time, generate weight between 1 and 100 kg
                    weight = random.nextInt(100) + 1;
                } else if (weightCategory < 99) {
                    // 9% of the time, generate weight between 101 and 1000 kg
                    weight = random.nextInt(900) + 101;
                } else {
                    // 1% of the time, generate weight between 1001 and 2600 kg
                    weight = random.nextInt(1600) + 1001;
                }

                int shippingOrderId;
                int shippingOrderCount;
                do {
                    shippingOrderId = random.nextInt(750000) + 1;
                    shippingOrderCount = shippingOrderCounts.getOrDefault(shippingOrderId, 0);
                } while (shippingOrderCount >= (random.nextInt(5) + 1));

                shippingOrderCounts.put(shippingOrderId, shippingOrderCount + 1);
                // Generates only unique barcode ids.
                int barcodeId;
                do {
                    barcodeId = random.nextInt(750000) + 1;
                } while (usedBarcodeIds.contains(barcodeId));

                usedBarcodeIds.add(barcodeId);

                // Setting cost id according to the weight of the parcel
                int shippingCostId;
                if (weight <= 2) {
                    shippingCostId = 1;
                } else if (weight <= 20) {
                    shippingCostId = 2;
                } else if (weight <= 35) {
                    shippingCostId = 3;
                } else if (weight <= 1000) {
                    shippingCostId = 4;
                } else if (weight <= 2500) {
                    shippingCostId = 5;
                } else {
                    shippingCostId = 6;
                }
                writer.write(i + "," + weight + "," + shippingOrderId + "," + shippingCostId + "," + barcodeId + "\n");
            }
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
