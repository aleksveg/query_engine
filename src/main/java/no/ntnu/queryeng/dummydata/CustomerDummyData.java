package no.ntnu.queryeng.dummydata;
import com.github.javafaker.Address;
import com.github.javafaker.Faker;
import com.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Populates dummy customer data to use in customer table in PostNord database.
 */
public class CustomerDummyData {
    private static final int NUM_ORDERS = 10;
    public static void main(String[] args) throws IOException {
        Faker faker = new Faker(new Locale("nb-NO"));
        String[] header = {"id","first_name","last_name","address","post_code","city","mobile","email"};
        List<String[]> data = new ArrayList<>();
        int id = 1;

        while (id < NUM_ORDERS) {
            String firstName = faker.name().firstName();
            String lastName = faker.name().lastName();
            Address address = faker.address();
            String streetAddress = generateStreetAddress(faker);
            String postalCode = generateNorwegianPostalCode(faker);
            String city = address.city();
            String mobile;
            // Makes sure that mobile numbers are of length 8, and do not
            // start with 0 and 1.
            do {
                mobile = faker.phoneNumber().subscriberNumber(8);

            } while (mobile.length() != 8 || mobile.startsWith("0") || mobile.startsWith("1"));

            // Generates unique email address
            String email = faker.internet().emailAddress(firstName.toLowerCase() + "." + lastName.toLowerCase() + UUID.randomUUID().toString().replace("-", "").substring(0, 12));
            String[] row = {String.valueOf(id),firstName,lastName,streetAddress, postalCode, city, mobile,email};
            data.add(row);
            id++;

        }
        // Writes created data to a csv file.
        CSVWriter writer = new CSVWriter(new FileWriter("customer.csv"));
        writer.writeNext(header);
        writer.writeAll(data);
        writer.close();
    }

    /**
     * Generates postal code that resembles norwegian post codes
     * @param faker an instance of Faker
     * @return post code
     */
    private static String generateNorwegianPostalCode(Faker faker) {
        // Generate a random 4-digit number between 1000 and 9999
        int postalCode = faker.number().numberBetween(1000, 6666);
        return String.format("%04d", postalCode);
    }

    /**
     * Generates street address
     * @param faker an instance of Faker
     * @return street address
     */
    private static String generateStreetAddress(Faker faker) {
        String streetAddress;
        do {
            streetAddress = faker.address().streetAddress();
        } while (streetAddress.endsWith("0"));
        return streetAddress;
    }

}