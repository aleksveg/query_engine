package no.ntnu.queryeng.dummydata;

import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Populates dummy data to use in barcode table in PostNord database.
 */
public class BarCodeDummyData {

    public static void main(String[] args) throws IOException {
        Random rand = new Random();
        // Using hashset to make sure barcodes are unique
        Set<String> barcodes = new HashSet<>();
        List<String[]> data = new ArrayList<>();
        String[] header = {"id", "barcode", "in_use"};
        int id = 1;
        while (barcodes.size() < 1000000) {
            StringBuilder sb = new StringBuilder();
            // Adds 70 to indicate that barcodes represents postal service.
            sb.append("70");
            for (int i = 0; i < 21; i++) {
                sb.append(rand.nextInt(10));
            }
            String barcode = sb.toString();
            boolean value = true;
            if (barcodes.size() >= 750000) {
                value = false;
            }
            String[] row = {Integer.toString(id), barcode, Boolean.toString(value)};
            barcodes.add(barcode);
            data.add(row);
            id++;
        }

        // Writes the populated data to a csv file
        CSVWriter writer = new CSVWriter(new FileWriter("barcode.csv"));
        writer.writeNext(header);
        writer.writeAll(data);
        writer.close();
    }
}
