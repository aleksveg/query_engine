package no.ntnu.queryeng.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Represents a test for the ThirdParty class in the query engine application.
 *
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 24.04.2023
 */
class ThirdPartyTest {
    /**
     * Creates a xxxxxx object.
     */
    ThirdParty thirdParty;

    /**
     * Initializes the objects before the test gets ran.
     */
    @BeforeEach
    void setUp() {
        thirdParty = new ThirdParty(4, "Jens Ågesen", "Jens Olavs Veg 3", 6090, 48091273, "test@test.no");
    }

    /**
     * Testing if getId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Testing getId() method")
    void testGetId() {
        assertEquals(4, thirdParty.getId());
    }

    /**
     * Testing if getName() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Testing getName() method")
    void testGetName() {
        assertEquals("Jens Ågesen", thirdParty.getName());
    }

    /**
     * Testing if getAddress() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Testing getAddress() method")
    void testGetAddress() {
        assertEquals("Jens Olavs Veg 3", thirdParty.getAddress());
    }

    /**
     * Testing if getPostalCode() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Testing getPostalCode() method")
    void testGetPostalCode() {
        assertEquals(6090, thirdParty.getPostalCode());
    }

    /**
     * Testing if getMobile() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Testing getMobile() method")
    void testGetMobile() {
        assertEquals(48091273, thirdParty.getMobile());
    }

    /**
     * Testing if getEmail() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Testing getEmail() method")
    void testGetEmail() {
        assertEquals("test@test.no", thirdParty.getEmail());
    }

    /**
     * Testing if getInvoices() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Testing getInvoices() method")
    void testGetInvoices() {
        assertEquals(List.of(), thirdParty.getInvoices());
    }

    /**
     * Testing if getAdditionalServiceInvoices() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Testing getAdditionalServiceInvoices() method")
    void testGetAdditionalServiceInvoices() {
        assertEquals(List.of(), thirdParty.getAdditionalServiceInvoices());
    }
}