package no.ntnu.queryeng.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.sql.Date;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Represents a test for the Scan class in the query engine application.
 *
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 24.04.2023
 */
class ScanTest {
    /**
     * Creates a scan object.
     */
    Scan scan;
    /**
     * Creates a format object.
     */
    DateTimeFormatter format;
    /**
     * Creates a parcel object.
     */
    Parcel parcel;
    /**
     * Creates a terminal object.
     */
    Terminal terminal;
    /**
     * Creates a status object.
     */
    Status status;
    /**
     * Creates a date object
     */
    Date date;

    /**
     * Initializes the objects before the test gets ran.
     */
    @BeforeEach
    void setUp()  {

        Date date = new Date(System.currentTimeMillis());
        format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        status = new Status(2, "Under transport");
        terminal = new Terminal(1, "Åndalsnes", "Strandgata 102",6300,"Åndalsnes",(float) 62.563122192292425,(float) 7.674044951777277, 30);
        parcel = new Parcel(7, 30);
        scan = new Scan(9, parcel, terminal, status, date);
    }

    /**
     * Testing if getId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    void testGetId() {
        assertEquals(9, scan.getId());
    }

    /**
     * Testing if getParcelId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    void testGetParcelId() {
        assertEquals(7, scan.getParcel().getId());
    }

    /**
     * Testing if getTerminal() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    void testGetTerminal() {
        assertEquals(terminal.getName(), scan.getTerminal().getName());
    }

    /**
     * Testing if getStatus() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    void testGetStatus() {
        assertEquals(status.getStatus(), scan.getStatus().getStatus());
    }

    /**
     * Testing if getScannedAt() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    void testGetScannedAt() {
        assertEquals(LocalDateTime.now().format(format), scan.getScannedAt().toString());
    }

    /**
     * Testing if getParcel() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    void testGetParcel() {
        assertEquals(parcel, scan.getParcel());
    }
}