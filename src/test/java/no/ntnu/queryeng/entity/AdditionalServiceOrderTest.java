package no.ntnu.queryeng.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Represents a test for the AdditionalServiceOrder class in the query engine application.
 *
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 24.04.2023
 */
class AdditionalServiceOrderTest {
    /**
     * Creates a ShippingOrder object.
     */
    ShippingOrder shippingOrder = new ShippingOrder(1, LocalDateTime.now());
    ;
    /**
     * Creates a AdditionalService object.
     */
    AdditionalService additionalService = new AdditionalService(1, "Change of delivery address", "Deliver to kongensgate 3", 30.3);
    ;
    /**
     * Creates a AdditionalServiceOrder object.
     */
    AdditionalServiceOrder additionalServiceOrder = new AdditionalServiceOrder(0, shippingOrder.getId(), additionalService, LocalDateTime.now());
    /**
     * Creates a DateTimeFormatter object.
     */
    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

    /**
     * Testing if getId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    void testGetId() {
        assertEquals(0, additionalServiceOrder.getId());
    }

    /**
     * Testing if getShippingOrderId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    void testGetShippingOrderId() {
        assertEquals(shippingOrder.getId(), additionalServiceOrder.getShippingOrderId());
    }

    /**
     * Testing if getAdditionalService() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    void testGetAdditionalService() {
        assertEquals(additionalService, additionalServiceOrder.getAdditionalService());
    }

    /**
     * Testing if getAdditionalServiceInvoice() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    void testGetAdditionalServiceInvoice() {
        assertNull(additionalServiceOrder.getAdditionalServiceInvoice());
    }

    /**
     * Testing if getCreatedAt() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    void testGetCreatedAt() {
        assertEquals(LocalDateTime.now().format(format), additionalServiceOrder.getCreatedAt().format(format));
    }
}