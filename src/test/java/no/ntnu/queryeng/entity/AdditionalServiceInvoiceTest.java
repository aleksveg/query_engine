package no.ntnu.queryeng.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Represents a test for the AdditionalServiceInvoice class in the query engine application.
 *
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 24.04.2023
 */
class AdditionalServiceInvoiceTest {

    /**
     * Creates a AdditionalServiceInvoice object.
     */
    AdditionalServiceInvoice additionalServiceInvoice;

    /**
     * Creates a AdditionalServiceOrder object.
     */
    AdditionalServiceOrder additionalServiceOrder;

    /**
     * Creates a AdditionalService object.
     */
    AdditionalService additionalService;

    /**
     * Creates a ThirdParty object.
     */
    ThirdParty thirdParty;

    /**
     * Creates a DateTimeFormatter object.
     */
    DateTimeFormatter format;

    /**
     * Creates a ShippingOrder object.
     */
    ShippingOrder shippingOrder;

    /**
     * Initializes the objects before the test gets ran.
     */
    @BeforeEach
    void setUp() {
        format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        shippingOrder = new ShippingOrder(1, LocalDateTime.now());
        additionalService = new AdditionalService(1, "Change of delivery address", "Deliver to kongensgate 3", 30.3);
        additionalServiceOrder = new AdditionalServiceOrder(1, shippingOrder.getId(), additionalService, LocalDateTime.now());
        additionalServiceInvoice = new AdditionalServiceInvoice(1, additionalServiceOrder, LocalDateTime.now(), LocalDateTime.now().plusDays(1), false);
        thirdParty = new ThirdParty();
    }

    /**
     * Testing if getId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getId() method")
    void testGetId() {
        assertEquals(1, additionalServiceInvoice.getId());
    }

    /**
     * Testing if getAdditionalServiceOrder() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getAdditionalServiceOrder() method")
    void testGetAdditionalServiceOrder() {
        assertEquals(additionalServiceOrder, additionalServiceInvoice.getAdditionalServiceOrder());
    }

    /**
     * Testing if getCustomerId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getCustomerId() method")
    void testGetCustomerId() {
        assertNull(additionalServiceInvoice.getCustomerId());
    }

    /**
     * Testing if getThirdParty() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getThirdParty() method")
    void testGetThirdParty() {
        assertNull(additionalServiceInvoice.getThirdParty());
    }

    /**
     * Testing if getInvoicePayerTypeId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getInvoicePayerTypeId() method")
    void testGetInvoicePayerTypeId() {
        assertNull(additionalServiceInvoice.getInvoicePayerTypeId());
    }

    /**
     * Testing if getCreatedAt() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getCreatedAt() method")
    void testGetCreatedAt() {
        assertEquals(LocalDateTime.now().format(format), additionalServiceInvoice.getCreatedAt().format(format));
    }

    /**
     * Testing if getDueDate() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getDueDate() method")
    void testGetDueDate() {
        assertEquals(LocalDateTime.now().plusDays(1).format(format), additionalServiceInvoice.getDueDate().format(format));
    }

    /**
     * Testing if isPaid() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test isPaid() method")
    void testIsPaid() {
        assertFalse(additionalServiceInvoice.isPaid());
    }
}