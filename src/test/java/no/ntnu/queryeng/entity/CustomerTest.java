package no.ntnu.queryeng.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Represents a test for the Customer class in the query engine application.
 *
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 24.04.2023
 */
class CustomerTest {
    /**
     * Creates a xxxxxx object.
     */
    Customer customer;

    /**
     * Initializes the objects before the test gets ran.
     */
    @BeforeEach
    void setUp() {
        customer = new Customer(17, "Jens", "Pedersen", "Kong Håkons gade 432", "6090", "Volda", "47362821", "test@test.no");
    }

    /**
     * Testing if getId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getId() method")
    void testGetId() {
        assertEquals(17, customer.getId());
    }

    /**
     * Testing if getFirstName() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getFirstName() method")
    void testGetFirstName() {
        assertEquals("Jens", customer.getFirstName());
    }

    /**
     * Testing if getLastName() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getLastName() method")
    void testGetLastName() {
        assertEquals("Pedersen", customer.getLastName());
    }

    /**
     * Testing if getAddress() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getAddress() method")
    void testGetAddress() {
        assertEquals("Kong Håkons gade 432", customer.getAddress());
    }

    /**
     * Testing if getPostalCode() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getPostalCode() method")
    void testGetPostalCode() {
        assertEquals("6090", customer.getPostalCode());
    }

    /**
     * Testing if getCity() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getCity() method")
    void testGetCity() {
        assertEquals("Volda", customer.getCity());
    }

    /**
     * Testing if getMobile() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getMobile() method")
    void testGetMobile() {
        assertEquals("47362821", customer.getMobile());
    }

    /**
     * Testing if getEmail() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getEmail() method")
    void testGetEmail() {
        assertEquals("test@test.no", customer.getEmail());
    }

    /**
     * Testing if getSenderList() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getSenderList() method")
    void testGetSenderList() {
        assertEquals(List.of(), customer.getSenderList());
    }

    /**
     * Testing if getReceiverList() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getReceiverList() method")
    void testGetReceiverList() {
        assertEquals(List.of(), customer.getReceiverList());
    }

    /**
     * Testing if getInvoices() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getInvoices() method")
    void testGetInvoices() {
        assertEquals(List.of(), customer.getInvoices());
    }

    /**
     * Testing if getAdditionalServiceInvoices() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getAdditionalServiceInvoices() method")
    void testGetAdditionalServiceInvoices() {
        assertEquals(List.of(), customer.getAdditionalServiceInvoices());
    }
}