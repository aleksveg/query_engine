package no.ntnu.queryeng.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Represents a test for the invoice class in the query engine application.
 *
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 24.04.2023
 */
class InvoiceTest {
    /**
     * Creates a xxxxxx object.
     */
    ShippingOrder shippingOrder;
    /**
     * Creates a xxxxxx object.
     */
    InvoicePayerType invoicePayerType;
    /**
     * Creates a xxxxxx object.
     */
    ThirdParty thirdParty;
    /**
     * Creates a xxxxxx object.
     */
    Customer customer;
    /**
     * Creates a xxxxxx object.
     */
    Invoice invoice;

    /**
     * Initializes the objects before the test gets ran.
     */
    @BeforeEach
    void setUp() {
        Date date = new Date(System.currentTimeMillis());
        customer = new Customer(1, "Jens", "Pedersen", "Kong Håkons gade 432", "6090", "Volda", "47362821", "test@test.no");
        thirdParty = new ThirdParty();
        shippingOrder = new ShippingOrder(1, LocalDateTime.now());
        invoicePayerType = new InvoicePayerType(1, "Sender");
        invoice = new Invoice(11, shippingOrder, invoicePayerType, thirdParty, customer, date, date, true);
    }

    /**
     * Testing if getId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getId() method")
    void testGetId() {
        assertEquals(11, invoice.getId());
    }

    /**
     * Testing if getShippingOrderId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getShippingOrderId() method")
    void testGetShippingOrderId() {
        assertEquals(1, invoice.getShippingOrderId().getId());
    }

    /**
     * Testing if getInvoicePayerType() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getInvoicePayerType() method")
    void testGetInvoicePayerType() {
        assertEquals(invoicePayerType, invoice.getInvoicePayerType());
    }

    /**
     * Testing if getThirdParty() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getThirdParty() method")
    void testGetThirdParty() {
        assertEquals(thirdParty, invoice.getThirdParty());
    }

    /**
     * Testing if getCustomer() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getCustomer() method")
    void testGetCustomer() {
        assertEquals(customer, invoice.getCustomer());
    }
}