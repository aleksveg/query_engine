package no.ntnu.queryeng.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Represents a test for the Barcode class in the query engine application.
 *
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 24.04.2023
 */
class BarcodeTest {
    /**
     * Creates a xxxxxx object.
     */
    Barcode barcode;
    /**
     * Creates a xxxxxx object.
     */
    Parcel parcel;

    /**
     * Initializes the objects before the test gets ran.
     */
    @BeforeEach
    void setUp() {
        barcode = new Barcode(13, "1012398123", true);
        parcel = new Parcel(1, 30);
    }

    /**
     * Testing if getId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getId() method")
    void testGetId() {
        assertEquals(13, barcode.getId());
    }

    /**
     * Testing if getBarcode() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getBarcode() method")
    void testGetBarcode() {
        assertEquals("1012398123", barcode.getBarcode());
    }

    /**
     * Testing if isInUse() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test isInUse() method")
    void isInUse() {
        assertTrue(barcode.isInUse());
    }

    /**
     * Testing if getParcel() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getParcel() method")
    void testGetParcel() {
        assertNull(barcode.getParcel());
    }
}