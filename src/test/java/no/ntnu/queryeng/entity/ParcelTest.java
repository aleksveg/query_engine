package no.ntnu.queryeng.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Represents a test for the Parcel class in the query engine application.
 *
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 24.04.2023
 */
class ParcelTest {
    /**
     * Creates a non defined parcel.
     */
    Parcel parcel;

    /**
     * Initializes the objects before the test gets ran.
     */
    @BeforeEach
    void setUp() {
        parcel = new Parcel(1, 30);
    }

    /**
     * Testing if getId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getId() method")
    public void testGetId() {
        assertEquals(1, parcel.getId());
    }

    /**
     * Testing if getWeight() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getWeight() method")
    public void testGetWeight() {
        assertEquals(30, parcel.getWeight());
    }

    /**
     * Testing if creationOfParcelInstanceWithInvalidId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test creation of parcel with invalid id")
    public void testCreationOfParcelInstanceWithInvalidId() {
        Parcel parcel2;
        try {
            parcel2 = new Parcel(-2, 30);
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    /**
     * Testing if creationOfParcelInstanceWithInvalidWeight() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test creation of parcel with invalid weight")
    public void testCreationOfParcelInstanceWithInvalidWeight() {
        Parcel parcel2;
        try {
            parcel2 = new Parcel(3, -30);
        } catch (Exception e) {
            assertTrue(true);
        }
    }


}