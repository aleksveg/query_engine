package no.ntnu.queryeng.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.text.DecimalFormat;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Represents a test for the Terminal class in the query engine application.
 *
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 24.04.2023
 */
public class TerminalTest {
    /**
     * Creates a xxxxxx object.
     */
    private Terminal terminal;

    /**
     * Initializes the objects before the test gets ran.
     */
    @BeforeEach
    void setUp() {
        terminal = new Terminal(1, "Åndalsnes", "Strandgata 102", 6300, "Åndalsnes",62.563122192292425F ,7.674044951777277F, 30);
    }

    /**
     * Testing if getId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Testing getId() method")
    void testGetId() {
        assertEquals(1, terminal.getId());
    }

    /**
     * Testing if getName() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Testing getName() method")
    void testGetName() {
        assertEquals("Åndalsnes", terminal.getName());
    }

    /**
     * Testing if getAddress() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Testing getAddress() method")
    void testGetAddress() {
        assertEquals("Strandgata 102", terminal.getAddress());
    }

    /**
     * Testing if getPostalCode() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Testing getPostalCode() method")
    void testGetPostalCode() {
        assertEquals(6300, terminal.getPostalCode());
    }

    /**
     * Testing if getPostalName() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Testing getPostalName() method")
    void testPostalName() {
        assertEquals("Åndalsnes", terminal.getPostalName());
    }

    /**
     * Testing if getLatitude() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Testing getLatitude() method")
    void testGetLatitude() {
        assertEquals(62.563122192292425F, terminal.getLatitude());
    }

    /**
     * Testing if getLongitude() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Testing getLongitude() method")
    void testGetLongitude() {
        assertEquals(7.674044951777277F, terminal.getLongitude());
    }

    /**
     * Testing if getCost() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Testing getCost() method")
    void testGetCost() {
        assertEquals(30, terminal.getCost());
    }
}
