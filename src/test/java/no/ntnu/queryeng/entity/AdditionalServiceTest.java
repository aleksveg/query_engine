package no.ntnu.queryeng.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Represents a test for the AdditionalService class in the query engine application.
 *
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 24.04.2023
 */
class AdditionalServiceTest {
    /**
     * Creates a AdditionalService-object.
     */
    AdditionalService additionalService;

    /**
     * Initializes the objects before the test gets ran.
     */
    @BeforeEach
    void setUp() {
        additionalService = new AdditionalService(1, "Change of delivery address", "Deliver to kongensgate 3", 30.3);
    }

    /**
     * Testing if getId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getId() method")
    void testGetId() {
        assertEquals(1, additionalService.getId());
    }

    /**
     * Testing if getAdditionalServiceName() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getAdditionalServiceName() method")
    void testGetAdditionalServiceName() {
        assertEquals("Change of delivery address", additionalService.getAdditionalServiceName());
    }

    /**
     * Testing if getDescription() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getDescription() method")
    void testGetDescription() {
        assertEquals("Deliver to kongensgate 3", additionalService.getDescription());
    }

    /**
     * Testing if getCost() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getCost() method")
    void testGetCost() {
        assertEquals(30.3, additionalService.getCost());
    }

    /**
     * Testing if getAdditionalServiceOrder() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    @DisplayName("Test getAdditionalServiceOrder() method")
    void testGetAdditionalServiceOrder() {
        assertNull(additionalService.getAdditionalServiceOrder());
    }
}