package no.ntnu.queryeng.entity;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


/**
 * Represents a test for the ShippingOrder class in the query engine application.
 *
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 24.04.2023
 */
class ShippingOrderTest {
    /**
     * Creates a xxxxxx object.
     */
    private ShippingOrder shippingOrder;

    /**
     * Initializes the objects before the test gets ran.
     */
    @BeforeEach
    void setUp() {
        shippingOrder = new ShippingOrder(1, LocalDateTime.now());
    }

    /**
     * Test that the getDateAndTime method works as intended.
     * Verifies that the returns date and time is correct.
     */
    @Test
    @DisplayName("Test get date and time")
    void testGetDateAndTime() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        assertEquals(LocalDateTime.now().format(format), shippingOrder.getDateAndTime().format(format));
    }

    /**
     * Tests that the getId method works as intended.
     * Verifies that the value expected is the one returned.
     */
    @Test
    @DisplayName("Test get shippingOrder ID")
    void testGetId() {
        int id = 1;
        int shippingOrderId = shippingOrder.getId();
        assertEquals(id, shippingOrderId);
    }

    /**
     * Tests the getParcelList method works as intended.
     * Verifies that the expected list equals the returned list.
     */
    @Test
    @DisplayName("Test get parcel list")
    void testGetParcelList() {
        List<Parcel> parcelList = new ArrayList<>();
        Parcel parcel1 = new Parcel(1, 2);
        Parcel parcel2 = new Parcel(2, 25);
        parcelList.add(parcel1);
        parcelList.add(parcel2);
        shippingOrder.getParcelList().add(parcel1);
        shippingOrder.getParcelList().add(parcel2);
        List<Parcel> shippingOrderParcelList = shippingOrder.getParcelList();
        assertEquals(parcelList, shippingOrderParcelList);
    }
}