package no.ntnu.queryeng.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Represents a test for the ShippingCost class in the query engine application.
 *
 * @author Joakim Røren Melum
 * @version 0.1
 * @date 24.04.2023
 */
class ShippingCostTest {
    /**
     * Creates a xxxxxx object.
     */
    ShippingCost shippingCost;

    /**
     * Initializes the objects before the test gets ran.
     */
    @BeforeEach
    void setUp() {
        shippingCost = new ShippingCost(0, "Frakt til Ørsta", 30.4);
    }

    /**
     * Testing if getId() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    void testGetId() {
        assertEquals(0, shippingCost.getId());
    }

    /**
     * Testing if getDescription() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    void testGetDescription() {
        assertEquals("Frakt til Ørsta", shippingCost.getDescription());
    }

    /**
     * Testing if getCost() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    void testGetCost() {
        assertEquals(30.4, shippingCost.getCost());
    }

    /**
     * Testing if getParcels() method works as expected.
     * Test will <code>PASS</code> if the value expected is returned.
     * Test will <code>FAIL</code> if an unexpected value is returned.
     */
    @Test
    void testGetParcels() {
        assertEquals(List.of(), shippingCost.getParcels());
    }
}